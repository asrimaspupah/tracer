<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><span>@lang('app.name')</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix" style="
    margin-top: 50px;">
            <div class="profile_pic">
                <img src="{{ url('images/img.jpg') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{Session::get('username')}}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu" id="menuList">
                    <!--li> <a href="{{ url('/dasbor') }}"><i class="fa fa-users"></i> Dashboard </a></li-->
					<li><a><i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('/dasbor') }}"><i class="fa fa-clock-o"></i> Masa Tunggu Lulusan</a></li>
                            <li><a href="{{ url('/dasbor/level') }}"><i class="fa fa-suitcase"></i>Tempat Bekerja Lulusan</a></li>
							<li><a href="{{ url('/dasbor/posisi') }}"><i class="fa fa-check-square-o"></i>Kesesuaian Bidang Lulusan</a></li>
							<li><a href="{{ url('/dasbor/kepuasan') }}"><i class="fa fa-smile-o"></i>Kepuasan Pengguna Lulusan</a></li>
                        </ul>
                    </li>
                    <!-- <li> <a href="{{ url('/user') }}"><i class="fa fa-users"></i> Management Users </a></li>
                    <li><a><i class="fa fa-users"></i> Tracer Study <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="#"><i class="fa fa-database"></i>Master Alumni</a></li>
                            <li><a><i class="fa fa-gear"></i>Configure Tracer Study <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="#">Notification Schedule</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-file-o"></i>Report</a></li>
                        </ul>
                    </li> -->
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
<script>
var Status = true;
var APP_URL = {!! json_encode(url('/')) !!}
var UserId = {{Session::get('userid')}}
var menuList = {!! json_encode(Session::get('menuList')) !!}
//alert(JSON.stringify(menuList));
$("#menuList").empty();
var data = menuList;
var rowData = "";
var parentId = "";
var flagUl = false;
for (var index = 0; index < data.length ; index++){
    if (data[index].parentid == null){
        if (flagUl){
            flagUI = false
            rowData = rowData + " </ul>"
        }
        if (data[index].uri == null){
            rowData = rowData + "<li><a><i class='"+data[index].icon+"'></i>"+ data[index].menuname+" <span class='fa fa-chevron-down'></span></a>";
        }else{
            rowData = rowData + "<li><a href="+ APP_URL+ data[index].uri +"><i class='"+data[index].icon+"'></i> "+ data[index].menuname+"</a></li>";
        }
    }else{
        if (data[index].parentid == parentId){
            rowData = rowData + "<li><a href="+ APP_URL+ data[index].uri +"><i class='"+data[index].icon+"'></i> "+ data[index].menuname+"</a></li>";
        }else {
            rowData = rowData + "<ul class='nav child_menu'>"
            rowData = rowData + "<li><a href="+ APP_URL+ data[index].uri +"><i class='"+data[index].icon+"'></i> "+ data[index].menuname+"</a></li>";
            flagUl = true
            parentId = data[index].parentid
        }
    }        
}
$("#menuList").append(rowData);
    // $.ajax({
    //     type: "GET",
    //     url: APP_URL + "/api/auth/menu", 
    //     data: { 
    //     'userId': UserId
    //     },
    //     success: function(result){
    //         $("#menuList").empty();
    //         var data = result.data;
    //         var rowData = "";
    //         var parentId = "";
    //         var flagUl = false;
    //         for (var index = 0; index < data.length ; index++){
    //             if (data[index].parentid == null){
    //                 if (flagUl){
    //                     flagUI = false
    //                     rowData = rowData + " </ul>"
    //                 }
    //                 if (data[index].uri == null){
    //                     rowData = rowData + "<li><a><i class='"+data[index].icon+"'></i>"+ data[index].menuname+" <span class='fa fa-chevron-down'></span></a>";
    //                 }else{
    //                     rowData = rowData + "<li><a href="+ APP_URL+ data[index].uri +"><i class='"+data[index].icon+"'></i> "+ data[index].menuname+"</a></li>";
    //                 }
    //             }else{
    //                 if (data[index].parentid == parentId){
    //                     rowData = rowData + "<li><a href="+ APP_URL+ data[index].uri +"><i class='"+data[index].icon+"'></i> "+ data[index].menuname+"</a></li>";
    //                 }else {
    //                     rowData = rowData + "<ul class='nav child_menu'>"
    //                     rowData = rowData + "<li><a href="+ APP_URL+ data[index].uri +"><i class='"+data[index].icon+"'></i> "+ data[index].menuname+"</a></li>";
    //                     flagUl = true
    //                     parentId = data[index].parentid
    //                 }
    //             }        
    //         }
    //         $("#menuList").append(rowData);
    //     }
    // });
</script>