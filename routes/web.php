<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'LoginController@login');
Route::get('/', 'LoginController@login');
Route::post('/loginPost', 'LoginController@loginPost');
Route::get('/logout', 'LoginController@logout');

Route::get('/test', 'User\UserController@getDuplicateUser');
Route::group(['prefix' => 'user','middleware' => 'web'], function () {
    Route::get('/', 'User\UserController@index');
});

Route::get('/dasbor','Dashboard\DashboardController@index');
Route::get('/dasbor/kepuasan','Dashboard\DashboardController@kepuasan');
Route::get('/dasbor/level','Dashboard\DashboardController@levelPerusahaan');
Route::get('/dasbor/posisi','Dashboard\DashboardController@posisiPekerjaan');

Route::get('/beranda','Beranda\BerandaController@index');