<?php

namespace App\Query;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserQuery
{
    public function authentication($userid, $password){

        $id = $userid;
        $password = md5($password);
        $data = DB::table('user')->where('username', $userid)->first();
        if($data){ //apakah user id tersebut ada atau tidak
            $hashed = Hash::make($data->PASSWORD);
            if(Hash::check($password,$hashed)){
                $data = DB::table('user')
                        ->join('userinrole', 'user.userid', '=', 'userinrole.userid')
                        ->join('role', 'role.roleid', '=', 'userinrole.roleid')
                        ->select('user.*', 'userinrole.ROLEID', 'role.ROLENAME')
                        ->where('username', $userid)->first();
                $data -> status = 3;
                return $data;
            }else{
                $data -> status = 2;
                return $data; // error password tidak match;    
            }
        }else{
            $data -> status = 1;
            return $data; // error user id salah;
        }
    }
}
