@extends('core.common',
    [
        'page_title' => __('Dashboard'),
        'body_class' => 'nav-md'
    ])

@section('extra_styles')
    <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/jqvmap.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/dataTables.bootstrap.css') }}" rel="stylesheet" >
    <!-- Favicon -->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
    <link href="{{ asset('bsb/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="{{ asset('bsb/plugins/node-waves/waves.css') }}" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="{{ asset('bsb/plugins/animate-css/animate.css') }}" rel="stylesheet" />
    <!-- Morris Chart Css-->
    <link href="{{ asset('bsb/plugins/morrisjs/morris.css') }}" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('bsb/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('bsb/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <!-- Custom Css -->
    <link href="{{ asset('bsb/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('bsb/css/custom.style.css') }}" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('bsb/css/themes/all-themes.css') }}" rel="stylesheet" />

@endsection

@section('extra_scripts')
<script src="{{ asset('js/fastclick.js') }}"></script>
<script src="{{ asset('js/nprogress.js') }}"></script>
<!-- <script src="{{ asset('js/chart.js') }}"></script> -->
<script src="{{ asset('js/gauge.js') }}"></script>
<script src="{{ asset('js/bootstrap-progressbar.js') }}"></script>
<script src="{{ asset('js/icheck.js') }}"></script>
<script src="{{ asset('js/skycons.js') }}"></script>
<script src="{{ asset('js/jquery.flot.js') }}"></script>
<script src="{{ asset('js/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('js/jquery.flot.time.js') }}"></script>
<script src="{{ asset('js/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('js/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('js/date.js') }}"></script>
<script src="{{ asset('js/jqvmap.js') }}"></script>
<script src="{{ asset('js/jqvmap.world.js') }}"></script>
<script src="{{ asset('js/jqvmap.sampledata.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/bootstrap-daterangepicker.js') }}"></script>
<script src="{{ asset('js/kustom.js') }}"></script>
{{-- <script src="{{ asset('js/jquery.js') }}"></script> --}}
<!-- <script src="{{ asset('js/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/DataTables/dataTables.bootstrap.min.js') }}"></script> -->
<script src="{{ asset('sweetalert/dist/sweetalert.min.js') }}"></script>

<!-- Bootstrap Core Js {Jangan dibuka nanti konflik dgn js bootsrap yg ada : kl engga, ga muncul tombol logout} -->
<!-- <script src="{{ asset('bsb/plugins/bootstrap/js/bootstrap.js') }}"></script> -->

<!-- Select Plugin Js -->
<script src="{{ asset('bsb/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{ asset('bsb/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ asset('bsb/plugins/node-waves/waves.js') }}"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="{{ asset('bsb/plugins/jquery-countto/jquery.countTo.js') }}"></script>

 <!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('bsb/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('bsb/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('bsb/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('bsb/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('bsb/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('bsb/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('bsb/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('bsb/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('bsb/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

<!-- ChartJs -->
<script src="{{ asset('bsb/plugins/chartjs/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('bsb/plugins/chartjs/utils.js') }}"></script>
<!-- Sparkline Chart Plugin Js -->
<script src="{{ asset('bsb/plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>

<!-- Custom Js -->
<!-- {jangan dibuka soalnya ngerubah elemen select, sebenrnya buat nambahin checklist di combobox} 
    akibatnya pas nambahan value select malah ga muncul krn dibikin 2 select, 
    pakai yang default gentelela saja}-->
<!-- <script src="{{ asset('bsb/js/admin.js') }}"></script> -->
<!-- <script src="{{ asset('bsb/js/pages/charts/chartjs.js') }}"></script> -->
<script src="{{ asset('bsb/js/pages/tables/jquery-datatable.js') }}"></script>
@endsection

@section('body_content')
    <div class="container body">
        <div class="main_container">
            @include('core.nav_left')
            @include('core.nav_top')
            @include('dashboard.posisi_content')
        </div>
    </div>
@endsection

<?php
print str_pad('',4096)."\n";
ob_flush();
flush();
set_time_limit(45);
?>