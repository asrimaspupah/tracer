<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Model\ModelUser;

class UserController extends Controller
{
    public function index(){
        if (!Session::get('login')) {
            return redirect('/')->with('alert', 'Kamu harus login dulu');
        }else{
            return view('user/index', ['masterRole' => $this->getMasterRole()]);
        }
    }
    // public function indexAdd(){
    //     if (!Session::get('login')) {
    //         return redirect('login')->with('alert', 'Kamu harus login dulu');
    //     }else{
    //         return view('user/indexAdd', ['user' => $this->getDuplicateUser(), 'masterRole' => $this->getMasterRole()]);
    //     }
    // }

    public function getDuplicateUser(){
        $userDB = $this->showAllUser();
        return $userDB;
    }

    public function showAllUser(){
        $user = ModelUser::all();
        return $user;
    }
    public function getRole(){
        $role = array();
        $temp = DB::table('role')->select('id', 'name')->get();
        $user = $this->showAllUser();
        for($i=0;$i<count($user);$i++){
            for($j=0;$j<count($temp);$j++){
                if($user[$i]->role == $temp[$j]->id){
                    $role[$i] = $temp[$j]->name;
                }
            }
        }
        return $role;
    }
    public function getMasterRole(){
        $temp = DB::table('role')->select('ROLEID', 'ROLENAME')->get();
        return $temp;
    }
    public function ubahRole(Request $request){
        $user = ModelUSer::find($request->id);
        $user->role = $request->role;
        $user->updated_by = Session::get('nama');
        $user->save();
        return redirect('user')->with('alert', 'User Telah Diubah');
    }
    
    public function addUser(Request $result){
        $url = "http://192.168.4.13:8080/api/integreted/getProfile?NIK=".$result->nik;
        $json = json_decode(file_get_contents($url));
        ModelUser::create(
            ['nik' => $json->data->NIK,
             'nama' => $json->data->NAME,
             'password' => md5('bsp123!!'),
             'role' => $result->role,
             'created_by' => Session::get('nama'),
             'updated_by' => Session::get('nama')]
        );
        return redirect('user')->with('alert', 'User Telah Ditambahkan');
    }
    public function deleteUser($nik){
        $user = ModelUSer::find($nik);
        $user->delete();
        return redirect('user')->with('alert', 'User Telah Dihapus');
    }
}
