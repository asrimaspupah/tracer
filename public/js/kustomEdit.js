
$('#btn-tambah').click(function () {
    $(".modal-title").html("ADD");
    cleanModal();
    showModal();
});

function cleanModal(){
    $("#_id").val("");
    $("#nama-komponen").val("");
    document.forms["forms"]["opt2"].checked=true;
    document.forms["forms"]["opt4"].checked=true;
    if($('#opt4').is(':checked')) { dariRupiah() }
    $("#value").val("");
    $("#description").val("");
    
}
function getData(id){
    var request = new XMLHttpRequest()
    var url = 'component/search/'+id
    request.open('GET', url, true)
    request.onload = function() {
    // Begin accessing JSON data here
    var data = JSON.parse(this.response)
        if (request.status >= 200 && request.status < 400) {
            editModalMasterPayroll(id,data.component_type,data.component_unit,data.from,data.component_name,data.component_value,data.component_description)
        } else {
            console.log('error')
        }
    }
    request.send()
}

function showModal() {
    $('#modal').modal({
        backdrop: 'static',
        keyboard: true,
        show: true
    });
    $('#modal').on('shown.bs.modal', function () {
        $('#nama-komponen').focus();
    });
}

function dialogConfirm(message, yesCallback, noCallback) {
    $('#modal_dialog').modal({
        backdrop: 'static',
        keyboard: true,
        show: true
    });
    $('.title').html(message);
    var dialog = $('#modal_dialog').dialog();

    $('#btnYes').click(function() {
        dialog.dialog('close');
        yesCallback();
    });
    $('#btnNo').click(function() {
        dialog.dialog('close');
        noCallback();
    });
}
function ubahRole(nik) {
    $('#id').val(nik)
    $(".modal-title").html("Change");
    showModal();
}
function addUser(id) {
    $('#nik').val(id)
    $(".modal-title").html("Add");
    showModal();
}

function dariPersen() {
    document.getElementById('select').style.display = 'block'
    document.getElementById('label-unit').innerHTML = 'Value(%)';
}

function dariRupiah() {
    document.getElementById('select').style.display = 'none';
    document.getElementById('label-unit').innerHTML = 'Value(Rp.)';
}

function resetSelect() {
    $('select').each(function () {
        var $this = $(this),
            numberOfOptions = $(this).children('option').length;

        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');

        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        var $listItems = $list.children('li');

        $styledSelect.click(function (e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function () {
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
        });

        $listItems.click(function (e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            //console.log($this.val());
        });
        $(document).click(function () {
            $styledSelect.removeClass('active');
            $list.hide();
        });

    });
}

function fungsi(elem) {
    var ids = $(elem).attr("id");
    if (elem.checked) {
        document.getElementById('i' + ids).disabled = false;
    } else {
        document.getElementById('i' + ids).disabled = true;
    }
}

$(".rupiah").autoNumeric('init', {
    aSep: '.',
    aDec: ',',
    aForm: true,
    vMax: '9999999999',
    vMin: '-9999999999'
});

$('.NoIcon').MonthPicker({
    Button: false
});
$('#addEmployee').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('New message to ' + recipient)
    modal.find('.modal-body input').val(recipient)
});
$('.search').keydown(function () {
    $('tr.item:containsIgnoreCase("' + $(this).val() + '")').prependTo('#content');
});

function deleteData(id) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover it",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            swal("Data Deleted Succsesfully!", {
                icon: "success",
                buttons: false,
                });
                var form = "form-delete="+id;
                document.getElementById(form).submit();
            }
        });
}
function validateData(){
    var required = false; 
    $('#forms').find('input').each(function(){
        if($(this).prop('required')){
            if(!$(this).val()){
                required = true; 
            }
        }
    });
    if(!$('#description').val()){
        required = true; 
    }
    return required;
}
function addData(required) {
    console.log($('#_id').val());
    if(required){
        swal({
            title: "You have to Input All Coloumn",
            icon: "warning",
            dangerMode: true,
            });
    } else{
        swal({
            title: "Are you sure?",
            text: "You Can Edit It Later",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                swal("Data Added Succsesfully!", {
                    icon: "success",
                    buttons: false,
                });
                var form = "forms";
                document.getElementById(form).submit();
            }
        });
    }
}