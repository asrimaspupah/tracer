 <div class="right_col" role="main">
     <div class="container-fluid">
         <div id="breadcrumbs-wrapper">
             <ol class="breadcrumb breadcrumb-bg">
                 <li><i class="fa fa-users"></i><a href="{{ url('/dasbor') }}" style="text-decoration: none;"> Dashboard <span style="margin:0 10px;">></span> <i
                         class="fa fa-database"></i><a href="{{ url('/dasbor/kepuasan') }}" style="text-decoration: none;"> Tempat Kerja</a></li>
             </ol>
         </div>
         <div class="row clearfix">
             <!-- Bar Chart Kepuasan-->
             <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                 <div class="card">
                     <div class="header">
                         <h2>Level Perusahaan Lulusan</h2>
                         <ul class="header-dropdown m-r--5">
                             <li class="dropdown">
                                 <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                     role="button" aria-haspopup="true" aria-expanded="false">
                                     <i class="material-icons">more_vert</i>
                                 </a>
                                 <ul class="dropdown-menu pull-right">
                                     <li><a href="javascript:void(0);">Unduh</a></li>
                                 </ul>
                             </li>
                         </ul>
                     </div>
                     <div class="body">
                         <canvas id="barChart" ></canvas>
                     </div>
                 </div>
             </div>
             <!-- #END# Bar Chart Kepuasan-->
             <!-- Filtering -->
             <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                 <div class="card">
                     <div class="body">
                         <div class="">
                             <p><b>Jurusan</b></p>
                             <select id="major" onchange="getProdi();" class="form-control show-tick">
                                 <!-- <option value="">-- Pilih Jurusan --</option> -->
                                 @foreach ($major as $item)
                                 <option value="{{$item->MAJORID}}">{{$item->MAJORNAME}}</option>
                                 @endforeach
                             </select>
                         </div>
                         <div class="m-t-15">
                             <p><b>Program Studi</b></p>
                             <select id="prodidropdown" class="form-control show-tick" onchange="getStackBar(this);">
                                 <!-- <option value="">-- Pilih Program Studi --</option> -->
                             </select>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Filtering -->
             <!-- Exportable Table -->
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="card">
                     <div class="header">
                         <h2>
                             Data detail
                         </h2>
                     </div>
                     <div class="body">
                         <div class="table-responsive">
                             <table
                                 class="table table-bordered table-striped table-hover dataTable js-exportable table-custom">
                                 <thead>
                                    <tr>
                                         <th rowspan="2" class="col-md-1">Tahun</th>
                                         <th rowspan="2" class="col-md-1">Jumlah Lulusan</th>
                                         <th rowspan="2" class="col-md-2">Jumlah Lulusan yang Terlacak</th>
                                         <th rowspan="2" class="col-md-2">Jumlah Lulusan Berpindah Pekerjaan</th>
                                         <th colspan="3">Jumlah Lulusan Terlacak yang Bekerja Berdasarkan Tingkat/Ukuran Tempat Kerja/Berwirausaha</th>
                                     </tr>
                                     <tr>
                                         <th class="col-md-2">Lokal/ Wilayah/ Berwirausaha tidak Berbadan Hukum</th> 
                                         <th class="col-md-2">Nasional/ Berwirausaha Berbadan Hukum</th> 
                                         <th class="col-md-2">Multinasiona/ Internasional</th>
                                     </tr>
                                 </thead>
                                 <tbody id="mtMod">

                                 </tbody>
                             </table>
                         </div>
                     </div>
                 </div> -->
                <!-- #END# Exportable Table-->

             </div>
         </div>
     </div>
 </div>

 <!-- /page content -->

 {{-- Javascript --}}
 <script src="{{ asset('bsb/plugins/jquery/jquery.min.js') }}"></script>

 <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
    var myChart = null;
    var myTable = {};

$(function() {
    $("#major").trigger("change");

    myTable = $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'excel', 'pdf'
        ]
    });
});

function getProdi() {
    $.ajax({
        type: "GET",
        url: APP_URL + "/api/dasbor/cmbprodi",
        data: {
            'major': $('#major').val()
        },
        success: function(result) {
            var element = $('#prodidropdown');
            element.empty();
            // element.append("<option value=''> -- Pilih Program Studi -- </option>");
            var prodi = result.data;
            for (var i = 0; i < prodi.length; i++) {
                // POPULATE SELECT ELEMENT WITH JSON.
                element.append("<option value='" + prodi[i].PRODID + "'>" + prodi[i].PRODINAME +
                    "</option>");
            }
            $('#prodidropdown').trigger("change");
        }
    });
}

function getStackBar(sel) {
    if (myChart != null) {
        myChart.destroy();
    }
    myTable.destroy();
    $("#mtMod").html('');

    var datalabel = [];  
    var tableString = "";

    $.ajax({
        type: "GET",
        url: APP_URL + "/api/dasbor/level/",
        data: {
            'prodi': $('#prodidropdown').val()
        },
        success: function(result) {
            var dataset = [];
            var nilai = [];
            var pindahkerja = [];
            var year;
            var labelyear = [];
            var percent = 0;
            var bgColor = ["#fec771", "#e6e56c", "#64e291"];
            var hbgColor = ["#e0b065", "#cfce63", "#5ccc84"];
            datalabel = JSON.parse(result.label[0].label);
            datalabelb = ["Lokal/ Wilayah/ Berwirausaha tidak Berbadan Hukum",
            "Nasional/ Berwirausaha Berbadan Hukum","Multinasiona/ Internasional"]

            for (var i = 0; i < result.level.length; i++){                
                labelyear.push(result.level[i].TAHUN);
            }
            for (var i = 0; i < datalabel.length; i++){
                var data = [];                
                for (var j = 0; j < result.level.length; j++){                    
                    nilai = result.level[j][datalabel[i]];
                    pindahkerja = result.level[j]["PINDAHKERJA"];
                    percent = (nilai/pindahkerja)*100;
                    data.push(percent);
                }
                dataset.push({label:datalabelb[i], data:data, backgroundColor:bgColor[i], hoverBackground:hbgColor[i]});
            }
            
            console.log(result);

            for (var i = 0; i < result.level.length; i++){
                tableString = tableString + "<tr>";
                tableString = tableString + "<td class='col-md-1'>" + result.level[i].TAHUN +
                    "</td><td class='col-md-1'>" + result.level[i].LULUSAN +
                    "</td><td class='col-md-1'>" + result.level[i].TERLACAK +
                    "</td><td class='col-md-1'>" + result.level[i].PINDAHKERJA + "</td>";
                    for (var j = 0; j < datalabel.length; j++){
                        tableString = tableString + "<td class='col-md-1'>" + result.level[i][datalabel[j]] + "</td>";
                    }
                tableString = tableString + "</tr>";    
            }

            console.log(tableString);

            var ctx = document.getElementById("barChart");
            myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labelyear,
                    datasets: dataset
                },
                options: {
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }            
            });

            $("#mtMod").append(tableString);
            myTable = $('.js-exportable').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'excel', 'pdf'
                ]
            });
        }
    });
}

 </script>
 {{-- Javascript --}}