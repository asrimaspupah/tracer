<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::enableForeignKeyConstraints();
        Schema::create('employee_components', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id',10);
            $table->string('nik',10);
            $table->foreign('nik')->references('nik')->on('employee')->onDelete('cascade');
            $table->string('id_component',10);
            $table->foreign('id_component')->references('id')->on('master_component')->onDelete('cascade');
            $table->integer('value',30);
            $table->string('created_by',20)->nullable();
            $table->string('updated_by',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_components');
    }
}
