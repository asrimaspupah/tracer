<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ModelUser extends Model
{
    public $incrementing = false;
    protected $table = 'user';
    protected $fillable = ['USERID','USERNAME','PASSWORD','PASSWORDSALT', 'LASTLOGIN', 'CREATEDBY','UPDATEBY'];
    protected $primaryKey = 'USERID';
    const CREATED_AT = 'CREATEDDATE';
    const UPDATED_AT = 'UPDATEDATE';

}
