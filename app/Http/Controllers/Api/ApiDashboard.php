<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Illuminate\Support\Facades\DB;

class ApiDashboard extends Controller 
{
    public $successStatus = 200;
    public $notFound = 404;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function getProdiByMajor(Request $request) 
    { 
        $input = $request->all(); 
        $prodi = DB::table('prodi')->Where('prodi.majorid',$input['major'])->get();
        return response()->json(['data' => $prodi], $this-> successStatus); 
    } 

    public function getMasaTungguByProdi(Request $request) 
    { 
        $input = $request->all(); 
        $pieMT = DB::table('v_masatunggupie')->Where('prodid',$input['prodi'])->get();
        $modMT = DB::table('v_masatunggu')->where('prodid',$input['prodi'])->get();
        return response()->json(['pie' => $pieMT,'mod' => $modMT], $this-> successStatus); 
    }
    
    public function getKepuasanByProdi(Request $request) 
    { 
        $input = $request->all(); 
        $prodi = $input['prodi'];

        $satMT = DB::select(DB::raw("CALL GetScaleRadioValue(29, $prodi)"));
        $quaMT = DB::select(DB::raw("CALL GetScaleRadioValue(30, $prodi)"));

        return response()->json(['sat' => $satMT,'qua' => $quaMT], $this->successStatus); 
    }

    public function getLevelByProdi(Request $request) 
    { 
        $input = $request->all(); 
        $prodi = $input['prodi'];
        $level = DB::select(DB::raw("CALL GetKesesuaianBidang(27, $prodi, 'LEVEL')"));
        $label = DB::select('SELECT getMultiOption(?,?) AS label', [27, 'LEVEL']);

        return response()->json(['level' => $level,'label'=>$label], $this->successStatus); 
    }

    public function getPosisiByProdi(Request $request) 
    { 
        $input = $request->all(); 
        $prodi = $input['prodi'];
        $posisi = DB::select(DB::raw("CALL GetKesesuaianBidang(27, $prodi, 'POSISI')"));
        $label = DB::select('SELECT getMultiOption(?,?) AS label', [27, 'POSISI']);

        return response()->json(['posisi' => $posisi,'label'=>$label], $this->successStatus); 
    }

    public function getLabelKepuasan(){
        $label = DB::table('scaleradiolabel')->where('referenceid',29)->get();
        $label2 = DB::table('scaleradiolabel')->where('referenceid',30)->get();
        return response()->json(['label' => [$label, $label2]], $this-> successStatus);
    }
}