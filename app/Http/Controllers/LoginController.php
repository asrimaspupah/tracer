<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Query\UserQuery;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    //
    public function index(){
        if (!Session::get('login')) {
            return redirect('/')->with('alert', 'Kamu harus login dulu');
        }else{
            return redirect('/dasbor');
        }
    }
    public function login(){
        return view('login/index');
    }

    public function loginPost(Request $request){

        $userid = $request->id;
        $password = $request->password;
        //$password = md5($request->password);
        //$data = ModelUser::where('id',$id)->first();
        $DBUser = new UserQuery(); 
        $data = $DBUser->authentication($userid, $password);
        if($data -> status > 1){ //apakah email tersebut ada atau tidak
            //$hashed = Hash::make($data->password);
            //if(Hash::check($password,$hashed)){
            if($data -> status == 3) {
                echo '<script>';
                echo 'console.log('. json_encode( $data ) .')';
                echo '</script>';
                Session::put('username',$data->USERNAME);
                Session::put('userid',$data->USERID);
                Session::put('userrole',$data->ROLEID);
                Session::put('login',TRUE);

                // get data menu
                $menuList = DB::table('function')
                ->join('roleinfunction','function.functionid','=','roleinfunction.functionid')
                ->join('userinrole','userinrole.ROLEID','=','roleinfunction.roleid');
                $menuList = $menuList->Where('userinrole.USERID',$data->USERID);
                $menuList = $menuList->select('function.functionid', 
                                    'function.menuname', 
                                    'function.uri', 
                                    'function.icon',
                                    'function.parentid')
                        ->orderBy('function.sequence', 'asc')->get();
                Session::put('menuList',$menuList);
                // end get menu
                if($data->ROLEID == 1){
                    return redirect('/user');
                }else{
                    return redirect('/beranda');
                }
                
            } else{
                return redirect('/')->with('alert','Password Salah !' );
            }
        }
        else{
            return redirect('/')->with('alert','Id, Salah!');
        }
    }

    public function logout(){
        Session::flush();
        return redirect('/')->with('alert','Kamu sudah logout');
    }
}
