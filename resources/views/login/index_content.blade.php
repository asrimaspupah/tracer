
    <div>
            @if(\Session::has('alert'))
                <div class="alert alert-danger">
                    <div>{{Session::get('alert')}}</div>
                </div>
            @endif
            @if(\Session::has('alert-success'))
                <div class="alert alert-success">
                    <div>{{Session::get('alert-success')}}</div>
                </div>
            @endif
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="/loginPost" method="post">
            {{ csrf_field() }}
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" placeholder="Id" required="" id="id" name="id"/>
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" id="password" name="password" />
              </div>
              <div>
              <button type="submit" class="btn btn-default submit">Login</button>
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Tracer Study!!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
