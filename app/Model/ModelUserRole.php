<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ModelUserRole extends Model
{
    public $incrementing = false;
    protected $table = 'userinrole';
    protected $fillable = ['USERID','ROLEID','CREATEDBY','UPDATEBY'];
    protected $primaryKey = 'USERID';
    const CREATED_AT = 'CREATEDDATE';
    const UPDATED_AT = 'UPDATEDATE';

}
