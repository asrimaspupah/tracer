<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('auth/menu', 'Api\ApiAuthController@getMenu');

Route::get('user/all', 'Api\ApiUserController@allUser');
Route::get('user/search', 'Api\ApiUserController@search');
Route::get('user/getById', 'Api\ApiUserController@getUserById');
Route::post('user/save', 'Api\ApiUserController@addUser');
Route::put('user/save', 'Api\ApiUserController@editUser');
Route::delete('user/delete', 'Api\ApiUserController@deleteUser');

Route::get('dasbor/cmbprodi', 'Api\ApiDashboard@getProdiByMajor');
Route::get('dasbor/masatunggu', 'Api\ApiDashboard@getMasaTungguByProdi');
Route::get('dasbor/kepuasan', 'Api\ApiDashboard@getKepuasanByProdi');
Route::get('dasbor/level', 'Api\ApiDashboard@getLevelByProdi');
Route::get('dasbor/posisi', 'Api\ApiDashboard@getPosisiByProdi');
Route::get('dasbor/labelkepuasan', 'Api\ApiDashboard@getLabelKepuasan');