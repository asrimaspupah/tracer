 <div class="right_col" role="main">
     <div class="container-fluid">
         <div id="breadcrumbs-wrapper">
             <ol class="breadcrumb breadcrumb-bg">
                 <li><i class="fa fa-users"></i><a href="{{ url('/dasbor') }}" style="text-decoration: none;"> Dashboard <span style="margin:0 10px;">></span> <i
                         class="fa fa-database"></i><a href="{{ url('/dasbor/kepuasan') }}" style="text-decoration: none;"> Kepuasan Pengguna</a></li>
             </ol>
         </div>
         <div class="row clearfix">
             <!-- Bar Chart Kepuasan-->
             <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                 <div class="card">
                     <div class="header">
                         <h2>Kepuasan Pengguna Lulusan</h2>
                         <ul class="header-dropdown m-r--5">
                             <li class="dropdown">
                                 <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                     role="button" aria-haspopup="true" aria-expanded="false">
                                     <i class="material-icons">more_vert</i>
                                 </a>
                                 <ul class="dropdown-menu pull-right">
                                     <li><a href="javascript:void(0);">Unduh</a></li>
                                 </ul>
                             </li>
                         </ul>
                     </div>
                     <div class="body">
                         <canvas id="barChart" height="150"></canvas>
                     </div>
                 </div>
             </div>
             <!-- #END# Bar Chart Kepuasan-->
             <!-- Filtering -->
             <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                 <div class="card">
                     <div class="body">
                         <div class="">
                             <p><b>Jurusan</b></p>
                             <select id="major" onchange="getProdi();" class="form-control show-tick">
                                 <!-- <option value="">-- Pilih Jurusan --</option> -->
                                 @foreach ($major as $item)
                                 <option value="{{$item->MAJORID}}">{{$item->MAJORNAME}}</option>
                                 @endforeach
                             </select>
                         </div>
                         <div class="m-t-15">
                             <p><b>Program Studi</b></p>
                             <select id="prodidropdown" class="form-control show-tick" onchange="getStackBar(this);">
                                 <!-- <option value="">-- Pilih Program Studi --</option> -->
                             </select>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Filtering -->
             <!-- Exportable Table -->
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="card">
                     <div class="header">
                         <h2>
                             Data detail
                         </h2>
                     </div>
                     <div class="body">
                         <div class="table-responsive">
                             <table
                                 class="table table-bordered table-striped table-hover dataTable js-exportable table-custom">
                                 <thead>
                                     <tr>
                                         <th rowspan="2" class="col-md-4">Jenis Kemampuan</th>                                         
                                         <th colspan="4">Tingkat Kepuasan Pengguna</th>
                                     </tr>
                                     <tr>
                                         <th class="col-md-2">Kurang Baik</th>
                                         <th class="col-md-2">Cukup Baik</th>
                                         <th class="col-md-2">Baik</th>
                                         <th class="col-md-2">Sangat Baik</th> 
                                     </tr>
                                 </thead>
                                 <tbody id="mtMod">

                                 </tbody>
                             </table>
                         </div>
                     </div>
                 </div>
                 <!-- #END# Exportable Table -->
                 <!-- Modal content-->
                 <!-- Modal -->
                 <div class="modal" id="myModal" role="dialog">
                     <div class="modal-dialog">

                         <!-- Modal content-->
                         <div class="modal-content">
                             <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title" id="modalname"></h4>
                             </div>
                             <div class="modal-body">
                                 <canvas id="detailline_chart"></canvas>
                             </div>
                             <div class="modal-footer">
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                             </div>
                         </div>

                     </div>
                 </div>

             </div>
         </div>
     </div>
 </div>

 <!-- /page content -->

 {{-- Javascript --}}
 <script src="{{ asset('bsb/plugins/jquery/jquery.min.js') }}"></script>

 <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
    var myChart = null;
    var labelKepuasan = [];
    var labelKemampuan = [];
    var scaleKepuasan = 0;
    var scaleKemampuan = 0;
    var myTable = {};

$(function() {
    $("#major").trigger("change");

    $.ajax({
        type: "GET",
        url: APP_URL + "/api/dasbor/labelkepuasan/",
        success: function(result) {
            labelKepuasan = result.label[0][0].label;
            // labelKemampuan = result.label[0][1].label;
            scaleKepuasan = result.label[0][0].SCALE;
            // scaleKemampuan = result.label[0][1].SCALE;
        }
    });

    myTable = $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'excel', 'pdf'
        ]
    });
});

function getProdi() {
    $.ajax({
        type: "GET",
        url: APP_URL + "/api/dasbor/cmbprodi",
        data: {
            'major': $('#major').val()
        },
        success: function(result) {
            var element = $('#prodidropdown');
            element.empty();
            // element.append("<option value=''> -- Pilih Program Studi -- </option>");
            var prodi = result.data;
            for (var i = 0; i < prodi.length; i++) {
                element.append("<option value='" + prodi[i].PRODID + "'>" + prodi[i].PRODINAME +
                    "</option>");
            }
            $('#prodidropdown').trigger("change");
        }
    });
}

function getStackBar(sel) {
    if (myChart != null) {
        myChart.destroy();
    }
    myTable.destroy();
    $("#mtMod").html('');

    var myString = "";
    var datachart = [];
    var datalabel = [];

    $.ajax({
        type: "GET",
        url: APP_URL + "/api/dasbor/kepuasan/",
        data: {
            'prodi': $('#prodidropdown').val()
        },
        success: function(result) {
            console.log(result);   
            
            // kepuasan
            var bgColor = ["#eb7070", "#fec771", "#e6e56c", "#64e291"];
            var hbgColor = ["#d16464", "#e0b065", "#cfce63", "#5ccc84"];
            var data = [];
            var dataset=[];
            var labelScale = ["Kurang Baik", "Cukup Baik", "Baik", "Sangat Baik"];
            var tableString = "";
            var terlacak = 0;
            var level = 0;
            var percent = 0;
            var labels = JSON.parse(labelKepuasan);
            for(var i = 1; i <=scaleKepuasan; i++){
                data =[];
                for(var j = 0; j < result.sat[0].totalArray; j++){
                    terlacak = result.sat[0]["terlacak"];
                    level = result.sat[0]["LVL_"+ j + "_" + i];
                    percent = ~~((level/terlacak)*100);
                    data.push(percent);                    
                }                
                dataset.push({label:labelScale[i-1], data:data, backgroundColor:bgColor[i-1], hoverBackgroundColor:hbgColor[i-1]});
            }

            console.log(dataset);

            for (var i = 0; i < labels.length; i++){
                tableString = tableString + "<tr>";
                tableString = tableString + "<td class='col-md-4 text-left'>" + labels[i] + "</td>";
                for (var j = 0; j < labelScale.length; j++){
                    tableString = tableString + "<td class='col-md-2'>" + dataset[j]["data"][i] + "</td>";
                }
                tableString = tableString + "</tr>";
            }

            var ctx = document.getElementById("barChart");
            myChart = new Chart(ctx, {
                type: 'horizontalBar',
                data: {
                    labels: JSON.parse(labelKepuasan),
                    datasets: dataset
                },
                options: {
                    scales: {
                        xAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Persen'
                            }
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }                
            });

            $("#mtMod").append(tableString);
            myTable = $('.js-exportable').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'excel', 'pdf'
                ]
            });
        }
    });
}

 </script>
 {{-- Javascript --}}