<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Illuminate\Support\Facades\DB;
use App\Model\ModelUser;
use App\Model\ModelUserRole;

class ApiUserController extends Controller 
{
    public $successStatus = 200;
    public $notFound = 404;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }
                $input = $request->all(); 
                $input['password'] = bcrypt($input['password']); 
                $user = User::create($input); 
                $success['token'] =  $user->createToken('MyApp')-> accessToken; 
                $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this-> successStatus); 
    }

    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 

    /** 
     * allUser Data api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function allUser() 
    { 
        $user = ModelUser::all();
        return response()->json(['data' => $user], $this-> successStatus); 
    } 

     /** 
     * search Data api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function search(Request $request) 
    { 
        $input = $request->all(); 
        $user = DB::table('user')
               ->join('userinrole','user.userid','=','userinrole.userid')
               ->join('role','role.roleid','=','userinrole.roleid');
        if (isset($input['keyword'])){
            $user = $user->Where('user.userid',$input['keyword']);
            $user = $user->OrWhere('user.username',$input['keyword']);
        }
        $user = $user->select('user.USERID', 'user.USERNAME', 'role.ROLEID', 'role.ROLENAME')->get();
        return response()->json(['data' => $user], $this-> successStatus); 
    } 


    /** 
     * getDataBy Id Data api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function getUserById(Request $request) 
    { 
        $input = $request->all(); 
        //$user = ModelUSer::find($input['userid']);
        $user = DB::table('user')
                        ->join('userinrole', 'user.userid', '=', 'userinrole.userid')
                        ->join('role', 'role.roleid', '=', 'userinrole.roleid')
                        ->select('user.*', 'userinrole.ROLEID', 'role.ROLENAME')
                        ->where('user.userid', $input['userid'])->first();
        if ($user == null){
            return response()->json(['data' => $user], $this-> notFound); 
        }
        return response()->json(['data' => $user], $this-> successStatus); 
    } 

     /** 
     * Add User Data api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function addUser(Request $request) 
    { 
        $input = $request->all(); 
        $user = ModelUser::create(
            [//'USERID' => $input['userid'],
             'USERNAME' => $input['username'],
             'PASSWORD' => md5('12345'),
             //'role' => $input['role'],
             'CREATEDBY' => $input['creator'],
             'UPDATEBY' => $input['creator']]
        );
        $userid = DB::table('user')->max('USERID');
        $role = ModelUserRole::create(
            ['USERID' => $userid,
             'ROLEID' => $input['userrole'],
             'CREATEDBY' => $input['creator'],
             'UPDATEBY' => $input['creator']]
        );

        return response()->json(['data' => $user], $this-> successStatus); 
    } 

     /** 
     * Edit User Data api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function editUser(Request $request) 
    { 
        $input = $request->all(); 

        //update data user
        $user = ModelUser::find($input['currentId']);
        if ($user == null){ // jika data tidak ada
            return response()->json(['data' => $user], $this-> notFound); 
        }
        //$user->USERID = $input['userid'];
        $user->USERNAME = $input['username'];
        $user->UPDATEBY = $input['creator'];
        $user->save();

        //update user role
        DB::table('userinrole')
            ->where('USERID', $input['currentId'])
            ->update(['ROLEID' => $input['userrole']]);

        return response()->json(['data' => $user], $this-> successStatus); 
    } 

     /** 
     * Delete User Data api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function deleteUser(Request $request) 
    { 
        $input = $request->all(); 
        // delete data pada tabel user
        $user = ModelUser::find($input['userid']);
        
        if ($user == null){ // jika data tidak ada
            return response()->json(['data' => $user], $this-> notFound); 
        }
        $user->delete();

        // delete data pada tabel role
        DB::table('userinrole')->where('USERID', '=', $input['userid'])->delete();

        return response()->json(['data' => $user], $this-> successStatus); 
    } 
}