@extends('core.common',
    [
        'page_title' => __('Beranda'),
        'body_class' => 'nav-md'
    ])

@section('extra_styles')
    <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/jqvmap.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/dataTables.bootstrap.css') }}" rel="stylesheet" >
    <link href="{{ asset('bsb/css/style.css') }}" rel="stylesheet">

@endsection

@section('extra_scripts')
<script src="{{ asset('js/fastclick.js') }}"></script>
<script src="{{ asset('js/nprogress.js') }}"></script>
<script src="{{ asset('js/chart.js') }}"></script>
<script src="{{ asset('js/gauge.js') }}"></script>
<script src="{{ asset('js/bootstrap-progressbar.js') }}"></script>
<script src="{{ asset('js/icheck.js') }}"></script>
<script src="{{ asset('js/skycons.js') }}"></script>
<script src="{{ asset('js/jquery.flot.js') }}"></script>
<script src="{{ asset('js/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('js/jquery.flot.time.js') }}"></script>
<script src="{{ asset('js/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('js/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('js/date.js') }}"></script>
<script src="{{ asset('js/jqvmap.js') }}"></script>
<script src="{{ asset('js/jqvmap.world.js') }}"></script>
<script src="{{ asset('js/jqvmap.sampledata.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/bootstrap-daterangepicker.js') }}"></script>
<script src="{{ asset('js/kustom.js') }}"></script>
{{-- <script src="{{ asset('js/jquery.js') }}"></script> --}}
<script src="{{ asset('js/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/DataTables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('body_content')
    <div class="container body">
        <div class="main_container">
            @include('core.nav_left')
            @include('core.nav_top')
            @include('beranda.index_content')
        </div>
    </div>
@endsection

<?php
print str_pad('',4096)."\n";
ob_flush();
flush();
set_time_limit(45);
?>