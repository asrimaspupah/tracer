<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Illuminate\Support\Facades\DB;

class ApiAuthController extends Controller 
{
    public $successStatus = 200;
    public $notFound = 404;
   

     /** 
     * search Data api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function getMenu(Request $request) 
    { 
        $input = $request->all(); 
        $user = DB::table('function')
               ->join('roleinfunction','function.functionid','=','roleinfunction.functionid')
               ->join('userinrole','userinrole.ROLEID','=','roleinfunction.roleid');
        $user = $user->Where('userinrole.USERID',$input['userId']);
        $user = $user->select('function.functionid', 
                              'function.menuname', 
                              'function.uri', 
                              'function.icon',
                              'function.parentid')
                ->orderBy('function.sequence', 'asc')->get();
        return response()->json(['data' => $user], $this-> successStatus); 
    } 


   
}