 <div class="right_col" role="main">
     <div class="container-fluid">
         <div id="breadcrumbs-wrapper">
             <ol class="breadcrumb breadcrumb-bg">
                 <li><i class="fa fa-users"></i> Dashboard <span style="margin:0 10px;">></span> <i
                         class="fa fa-database"></i><a href="{{ url('/dasbor') }}" style="text-decoration: none;"> Masa
                         Tunggu</a></li>
             </ol>
         </div>
         <div class="row clearfix">
             <!-- Pie Chart -->
             <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                 <div class="card">
                     <div class="header">
                         <h2>Masa Tunggu Lulusan</h2>
                         <ul class="header-dropdown m-r--5">
                             <li class="dropdown">
                                 <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                     role="button" aria-haspopup="true" aria-expanded="false">
                                     <i class="material-icons">more_vert</i>
                                 </a>
                                 <ul class="dropdown-menu pull-right">
                                     <li><a href="javascript:void(0);">Unduh</a></li>
                                 </ul>
                             </li>
                         </ul>
                     </div>
                     <div class="body">
                         <!-- <canvas id="pie_chart" height="150"></canvas> -->
                         <div id="pie_chart" class="flot-chart"></div>
                     </div>
                 </div>
             </div>
             <!-- #END# Pie Chart -->
             <!-- Filtering -->
             <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                 <div class="card">
                     <div class="body">
                         <div class="">
                             <p><b>Jurusan</b></p>
                             <select id="major" onchange="getProdi();" class="form-control show-tick">
                                 <!-- <option value="">-- Pilih Jurusan --</option> -->
                                 @foreach ($major as $item)
                                 <option value="{{$item->MAJORID}}">{{$item->MAJORNAME}}</option>
                                 @endforeach
                             </select>
                         </div>
                         <div class="m-t-15">
                             <p><b>Program Studi</b></p>
                             <select id="prodidropdown" class="form-control show-tick" onchange="getPie(this);">
                                 <!-- <option value="">-- Pilih Program Studi --</option> -->
                             </select>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Filtering -->
             <!-- Exportable Table -->
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="card">
                     <div class="header">
                         <h2>
                             Data detail
                         </h2>
                     </div>
                     <div class="body">
                         <div class="table-responsive">
                             <table
                                 class="table table-bordered table-striped table-hover dataTable js-exportable table-custom">
                                 <thead>
                                     <tr>
                                         <th rowspan="2" class="col-md-1">Tahun</th>
                                         <th rowspan="2" class="col-md-1">Jumlah Lulusan</th>
                                         <th rowspan="2" class="col-md-2">Jumlah Lulusan yang Terlacak</th>
                                         <th rowspan="2" class="col-md-2">Jumlah Lulusan yang Dipesan Sebelum Lulus</th>
                                         <th colspan="3">Jumlah Lulusan Terlacak dengan Waktu Tunggu Mendapatkan
                                             Pekerjaan</th>
                                     </tr>
                                     <tr>
                                         <th class="col-md-2">WT < 3 Bulan</th> <th class="col-md-2">3 < WT < 6
                                                     Bulan</th> <th class="col-md-2">WT > 6 Bulan</th>
                                     </tr>
                                 </thead>
                                 <tbody id="mtMod">

                                 </tbody>
                             </table>
                         </div>
                     </div>
                 </div>
                 <!-- #END# Exportable Table -->
                <!-- Modal content-->
                <!-- Modal -->
                <div class="modal" id="myModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" id="modalname"></h4>
                            </div>
                            <div class="modal-body">
                                <canvas id="detailline_chart"></canvas>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

             </div>
         </div>
     </div>
 </div>

 <!-- /page content -->

 {{-- Javascript --}}
 <script src="{{ asset('bsb/plugins/jquery/jquery.min.js') }}"></script>

 <script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!}
var myChart = null;
var myDetailChart = null;
var myTable = {};

$(function() {
    $("#major").trigger("change");

    myTable = $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'excel', 'pdf'
        ]
    });
});

function getProdi() {
    $.ajax({
        type: "GET",
        url: APP_URL + "/api/dasbor/cmbprodi",
        data: {
            'major': $('#major').val()
        },
        success: function(result) {
            var element = $('#prodidropdown');
            element.empty();
            // element.append("<option value=''> -- Pilih Program Studi -- </option>");
            var prodi = result.data;
            for (var i = 0; i < prodi.length; i++) {
                // POPULATE SELECT ELEMENT WITH JSON.
                element.append("<option value='" + prodi[i].PRODID + "'>" + prodi[i].PRODINAME +
                    "</option>");
            }
            $('#prodidropdown').trigger("change");
        }
    });
}

function getPie(sel) {
    if (myChart != null) {
        myChart.destroy();
    }
    myTable.destroy();
    $("#mtMod").html('');

    var myString = "";
    var datachart = [];
    var datalabel = [];
    var pieChartData = [];
    var dataTahun = [];
    var dataSebelumLulus = [];
    var dataBulan1 = [];
    var dataBulan2 = [];
    var dataBulan3 = [];

    $.ajax({
        type: "GET",
        url: APP_URL + "/api/dasbor/masatunggu/",
        data: {
            'prodi': $('#prodidropdown').val()
        },
        success: function(result) {
            var pieChartColors = [window.chartColors.red, window.chartColors.orange, window.chartColors
                .yellow, window.chartColors.green
            ];
            for (var i = 0; i < result.pie.length; i++) {
                datachart.push(result.pie[i].PERSEN);
                datalabel.push(result.pie[i].ANSWER);
                pieChartData[i] = {
                    label: result.pie[i].ANSWER,
                    data: result.pie[i].PERSEN,
                    color: pieChartColors[i]
                }
            }
            for (var i = 0; i < result.mod.length; i++) {
                dataTahun.push(result.mod[i].TAHUNLULUS);
                dataSebelumLulus.push(result.mod[i].SEBELUMLULUS);
                dataBulan1.push(result.mod[i].BULAN1);
                dataBulan2.push(result.mod[i].BULAN2);
                dataBulan3.push(result.mod[i].BULAN3);
                $("#mtMod").append("<tr><td class='col-md-1'>" + result.mod[i].TAHUNLULUS +
                    "</td><td class='col-md-1'>" + result.mod[i].JMLLULUSAN +
                    "</td><td class='col-md-1'>" + result.mod[i].JMLTERLACAK +
                    "</td><td class='col-md-1'>" + result.mod[i].SEBELUMLULUS +
                    "</td><td class='col-md-1'>" + result.mod[i].BULAN1 + "</td><td class='col-md-1'>" +
                    result.mod[i].BULAN2 + "</td><td class='col-md-1'>" + result.mod[i].BULAN3 +
                    "</td></tr>");
            }

            var dataDetail = [dataTahun, dataSebelumLulus, dataBulan1, dataBulan2, dataBulan3];

            myTable = $('.js-exportable').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'excel', 'pdf'
                ]
            });

            getPiePlot(pieChartData, dataDetail);
        }
    });
}

function getPieDetail(answer, dataDetail) {
    var datal = [];
    switch (String(answer)) {
        case "SEBELUM LULUS":
            datal = dataDetail[1];
            break;
        case "0-3 BULAN":
            console.log("masuk");
            datal = dataDetail[2];
            break;
        case "3-6 BULAN":
            datal = dataDetail[3];
            break;
        case "> 6 BULAN":
            datal = dataDetail[4];
            break;
        default:
            datal = [];
    }

    var config = null;
    config = {
        type: 'line',
        data: {
            labels: dataDetail[0],
            datasets: [{
                label: answer,
                data: datal,
                borderColor: 'rgba(0, 188, 212, 0.75)',
                backgroundColor: 'rgba(0, 188, 212, 0.3)',
                pointBorderColor: 'rgba(0, 188, 212, 0)',
                pointBackgroundColor: 'rgba(0, 188, 212, 0.9)',
                pointBorderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: false,
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 35,
                        stepSize: 5
                    },
                    scaleLabel: {
                        display: true,
                        labelString: "Jumlah Mahasiswa"
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: "Tahun"
                    }
                }]
            }
        }
    }

    return config;
}

function getChartJs(type, datachart, datalabel) {
    var config = null;
    if (type === 'pie') {
        config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: datachart,
                    backgroundColor: [window.chartColors.red,
                        window.chartColors.orange,
                        window.chartColors.yellow,
                        window.chartColors.green
                    ]
                }],
                labels: datalabel
            }
        }
    }
    return config;
}

function getPiePlot(pieChartData, dataDetail) {
    $.plot('#pie_chart', pieChartData, {
        series: {
            pie: {
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 3 / 4,
                    formatter: labelFormatter,
                    background: {
                        opacity: 0.5
                    }
                }
            }
        },
        legend: {
            show: false
        },
        grid: {
            hoverable: true,
            clickable: true
        }
    });

    $('.pieLabel').bind('click', function(e) {
        $("#pie_chart .overlay").trigger(e);
    });

    $("#pie_chart").bind("plothover", function(event, pos, obj) {
        if (!obj) {
            return;
        }
        percent = parseFloat(obj.series.percent).toFixed(2);

        var html = [];
        html.push(
            "<div style=\"flot:left;width:105px;height:20px;text-align:center;border:1px solid black;background-color:",
            obj.series.color, "\">",
            "<span style=\"font-weight:bold;color:white\">", obj.series.label, " (", percent, "%)</span>",
            "</div>");

        $("#showInteractive").html(html.join(''));
    });

    $("#pie_chart").bind("plotclick", function(event, pos, obj) {
        if (myDetailChart != null) {
            myDetailChart.destroy();
        }
        if (!obj) {
            return;
        }
        $("#myModal").modal();
        $("#modalname").text("Masa Tunggu Lulusan" + obj.series.label);
        myDetailChart = new Chart(document.getElementById("detailline_chart").getContext("2d"), getPieDetail(obj
            .series.label, dataDetail));
    });

    function labelFormatter(label, series) {
        return '<div style="font-size:8pt; text-align:center; padding:2px; color:white;">' + label + '<br/>' + Math
            .round(series.percent) + '%</div>';
    }
}
 </script>
 {{-- Javascript --}}