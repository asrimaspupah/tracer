<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="bsb/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="bsb/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="bsb/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="bsb/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="bsb/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="bsb/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="bsb/css/style.css" rel="stylesheet">
    <link href="bsb/css/custom.style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="bsb/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">PELACAKAN LULUSAN POLBAN</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="bsb/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sri Ratna Wulan</div>
                    <div class="email">sri.ratna@polban.ac.id</div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENU UTAMA</li>
                    <li class="active">
                        <a href="index.html">
                            <i class="material-icons">home</i>
                            <span>Beranda</span>
                        </a>
                    </li>
                    <li>
                        <a href="pages/typography.html">
                            <i class="material-icons">av_timer</i>
                            <span>Masa Tunggu Lulusan</span>
                        </a>
                    </li>
                    <li>
                        <a href="pages/helper-classes.html">
                            <i class="material-icons">work</i>
                            <span>Tempat Bekerja Lulusan</span>
                        </a>
                    </li>
                    <li>
                        <a href="pages/helper-classes.html">
                            <i class="material-icons">layers</i>
                            <span>Kesesuaian Bidang Lulusan</span>
                        </a>
                    </li>
                    <li>
                        <a href="pages/helper-classes.html">
                            <i class="material-icons">sentiment_satisfied_alt</i>
                            <span>Kepuasan Pengguna Lulusan</span>
                        </a>
                    </li>                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2019 <a href="javascript:void(0);">Tracer Study JTK</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="menu-user">
                <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
            </ul>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASBOR</h2>
            </div>
            <div class="row clearfix">
                <!-- Pie Chart -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Masa Tunggu Lulusan</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Unduh</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <canvas id="pie_chart" height="150"></canvas>
                        </div>
                    </div>
                </div>
                <!-- #END# Pie Chart -->
                <!-- Filtering -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">               
                    <div class="card">
                        <div class="body">      
                            <div class="">
                                <p><b>Jurusan</b></p>                    
                                <select class="form-control show-tick">
                                    <option value="">-- Pilih Jurusan --</option>
                                    <option value="10">Teknik Komputer dan Informatika</option>
                                </select>
                            </div>  
                            <div class="m-t-15">
                                <p><b>Program Studi</b></p>
                                <select class="form-control show-tick">
                                    <option value="">-- Pilih Program Studi --</option>
                                    <option value="10">D-III Teknik Informatika</option>
                                    <option value="10">D-IV Teknik Informatika</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Filtering -->                  
                <!-- Exportable Table -->                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Data detail
                            </h2>                            
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable table-custom">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="col-md-1">Tahun</th>
                                            <th rowspan="2" class="col-md-1">Jumlah Lulusan</th>
                                            <th rowspan="2" class="col-md-2">Jumlah Lulusan yang Terlacak</th>
                                            <th rowspan="2" class="col-md-2">Jumlah Lulusan yang Dipesan Sebelum Lulus</th>
                                            <th colspan="3">Jumlah Lulusan Terlacak dengan Waktu Tunggu Mendapatkan Pekerjaan</th>
                                        </tr>
                                        <tr>                                            
                                            <th class="col-md-2">WT < 3 Bulan</th>
                                            <th class="col-md-2">3 < WT < 6 Bulan</th>
                                            <th class="col-md-2">WT > 6 Bulan</th>
                                        </tr>
                                    </thead>                                   
                                    <tbody>
                                    @foreach($data as $d)
                                    <tr>
                                        <td class="col-md-1">{{ $d->TAHUNLULUS }}</td>
                                        <td class="col-md-1">{{ $d->JMLLULUSAN }}</td>
                                        <td class="col-md-2">{{ $d->JMLTERLACAK }}</td>
                                        <td class="col-md-2">{{ $d->SEBELUMLULUS }}</td>
                                        <td class="col-md-2">{{ $d->BULAN1 }}</td>
                                        <td class="col-md-2">{{ $d->BULAN2 }}</td>
                                        <td class="col-md-2">{{ $d->BULAN3 }}</td>                                      
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>                
                <!-- #END# Exportable Table -->             
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="bsb/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="bsb/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="bsb/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="bsb/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="bsb/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="bsb/plugins/jquery-countto/jquery.countTo.js"></script>

     <!-- Jquery DataTable Plugin Js -->
    <script src="bsb/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="bsb/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="bsb/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="bsb/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="bsb/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="bsb/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="bsb/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="bsb/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="bsb/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- ChartJs -->
    <script src="bsb/plugins/chartjs/Chart.bundle.js"></script>
 
    <!-- Sparkline Chart Plugin Js -->
    <script src="bsb/plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="bsb/js/admin.js"></script>
    <script src="bsb/js/pages/charts/chartjs.js"></script>
    <script src="bsb/js/pages/tables/jquery-datatable.js"></script>

    <script>
        $(function () {
            new Chart(document.getElementById("pie_chart").getContext("2d"), getChartJs('pie'));
        });

        var datachart = [];
        var datalabel = [];
        @foreach($pie as $p)
            datachart.push({{ $p->PERSEN }});
            datalabel.push({{json_encode($p->ANSWER)}});
        @endforeach

        console.log(datachart);

        function getChartJs(type) {
            var config = null;
            if (type === 'pie') {
                config = {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: datachart,
                            backgroundColor: [
                                "rgb(233, 30, 99)",
                                "rgb(255, 193, 7)",
                                "rgb(0, 188, 212)",
                                "rgb(139, 195, 74)"
                            ],
                        }],
                        labels: datalabel
                    },
                    options: {
                        responsive: true,
                        legend: true
                    }
                }
            }
            return config;
        }
    </script>
</body>

</html>
