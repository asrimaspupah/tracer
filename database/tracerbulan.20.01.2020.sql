-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2020 at 08:28 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tracerbulan`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetKesesuaianBidang` (IN `refVal` INT(4), IN `prodi` INT(4), IN `alias` VARCHAR(20))  NO SQL
BEGIN
	DECLARE initQuery longtext;
    SET initQuery = concatSubQuestion(refVal,prodi,alias);
	SET @Query = initQuery;
	prepare b from @Query;
	execute b;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetMultiRadioValue` (`refVal` INT, `prodi` INT)  BEGIN
declare i int unsigned default 0;
declare totalArray int; 
declare initQuery varchar(1000);
SET initQuery = 'select YEAR(P.GRADUATIONDATE) AS YEAR, P.PRODID, R.SCALE * JML_TERLACAK(P.GRADUATIONDATE, P.PRODID) as terlacak,';
SET totalArray = (select JSON_LENGTH(a.answer) 
					FROM answer a, questionairefield q, alumnprofile p
                    where a.fieldid = q.fieldid 
						and p.userid = a.userid
						and p.prodid = prodi
						and q.referenceid = refVal limit 1);

while i < totalArray do
SET initQuery = concat(initQuery, 'SUM(JSON_EXTRACT(A.ANSWER, \'$[', i, ']\')) as \'', i, '\'');
IF totalArray - i > 1 THEN
SET initQuery = concat(initQuery, ', ');
END IF; 
SET i = i + 1;
end while;
SET initQuery = CONCAT(initQuery, ' FROM answer AS A 
									INNER JOIN ALUMNPROFILE AS P ON P.USERID = A.USERID 
                                    INNER JOIN QUESTIONAIREFIELD AS Q ON A.FIELDID = Q.FIELDID 
                                    INNER JOIN REFERENCECAT AS R ON Q.REFERENCEID = R.REFERENCEID
                                    WHERE YEAR(P.GRADUATIONDATE) >= YEAR(CURDATE())-5
										AND P.PRODID =',prodi,' AND Q.REFERENCEID=', refVal, ' GROUP BY P.GRADUATIONDATE, P.PRODID');
SET @Query = initQuery;
prepare b from @Query;
execute b;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetScaleRadioValue` (`refVal` INT, `prodi` INT)  BEGIN
declare initQuery longtext;

SET initQuery = (select concatMultiScale(refVal,prodi));

SET @Query = initQuery;
prepare b from @Query;
execute b;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `concatMultiScale` (`refVal` INT, `prodi` INT) RETURNS LONGTEXT CHARSET latin1 BEGIN
declare i int unsigned default 0;
declare j int unsigned default 1;
declare totalArray int; 
declare totalScale int;
declare initQuery longtext;

SET totalArray = (select JSON_LENGTH(a.answer) 
					FROM answer a, questionairefield q, alumnprofile p
                    where a.fieldid = q.fieldid 
						and p.userid = a.userid
						and p.prodid = prodi
						and q.referenceid = refVal limit 1);
SET totalScale = (select scale from referencecat where referenceid = refVal limit 1);

SET initQuery = CONCAT('select P.PRODID, JML_TERLACAK_PROD(P.PRODID) as terlacak, ',totalArray,' as totalArray ');
while i < totalArray do
	SET j = 1;
	while j <= totalScale do    
		SET initQuery = concat(initQuery, ', COUNT(CASE JSON_EXTRACT (A.ANSWER, \'$[', i, ']\') WHEN ',j,' THEN 1 ELSE NULL END) as LVL_',i,'_', j); 
        SET j = j + 1;
	end while;
	SET i = i + 1;
end while;

SET initQuery = CONCAT(initQuery, ' FROM answer AS A 
									INNER JOIN ALUMNPROFILE AS P ON P.USERID = A.USERID 
                                    INNER JOIN QUESTIONAIREFIELD AS Q ON A.FIELDID = Q.FIELDID 
                                    WHERE YEAR(P.GRADUATIONDATE) >= YEAR(CURDATE())-5
										AND P.PRODID =',prodi,' AND Q.REFERENCEID=', refVal, ' GROUP BY P.PRODID');

RETURN initQuery;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `concatSubQuestion` (`refVal` INT(4), `prodi` INT(4), `alias` VARCHAR(20)) RETURNS LONGTEXT CHARSET latin1 BEGIN
declare i int unsigned default 0;
declare j int unsigned default 0;
declare totalArray int; 
declare initQuery longtext;
DECLARE optionLevel JSON;
DECLARE optionLabel varchar(30);
SET optionLevel = getMultiOption(refVal, alias);
SET initQuery = '';
SET initQuery = CONCAT(initQuery,'SELECT YEAR(P.GRADUATIONDATE) AS TAHUN, JML_LULUSAN(P.GRADUATIONDATE, P.PRODID) AS LULUSAN,
    JML_TERLACAK(P.GRADUATIONDATE, P.PRODID) AS TERLACAK,
    COUNT(JSON_EXTRACT(A.ANSWER, CONCAT(''$['',I.id,''].',alias,'''))) AS PINDAHKERJA ');

SET optionLabel = '';
WHILE j < json_length(optionLevel) do 
	SET optionLabel = (SELECT JSON_EXTRACT(getMultiOption(refVal, alias),CONCAT('$[', j ,']')));
	SET initQuery = CONCAT(initQuery, ', COUNT(CASE JSON_EXTRACT(A.ANSWER, CONCAT(''$['',I.id,''].',alias,''')) WHEN \'', optionLabel, '\' THEN 1 ELSE NULL END) AS ', optionLabel, ' ');
    SET j = j + 1;
END WHILE;

SET initQuery = CONCAT(initQuery, 'FROM answer AS A INNER JOIN ALUMNPROFILE AS P ON P.USERID = A.USERID 
    INNER JOIN QUESTIONAIREFIELD AS Q ON A.FIELDID = Q.FIELDID 
    INNER JOIN REFERENCECAT AS R ON Q.REFERENCEID = R.REFERENCEID 
    JOIN(SELECT id from idx limit ', json_length(optionLevel),')
	AS I WHERE JSON_EXTRACT(A.ANSWER, CONCAT(''$['',I.id,'']'')) IS NOT NULL 
    AND YEAR(P.GRADUATIONDATE) >= YEAR(CURDATE())-5
    AND P.PRODID =',prodi,' AND Q.REFERENCEID=',refVal,'
    GROUP BY YEAR(P.GRADUATIONDATE)');
RETURN initQuery;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `getMultiOption` (`refVal` INT, `Alias` VARCHAR(20)) RETURNS LONGTEXT CHARSET utf8mb4 COLLATE utf8mb4_bin BEGIN
DECLARE length INT;
DECLARE result JSON;
select json_length(question, '$.SubQuestion') INTO length from questionairefield where referenceid = refVal;

SELECT JSON_EXTRACT(QUESTION, CONCAT('$.SubQuestion[',I.id,'].Options[*].Value')) INTO result FROM QUESTIONAIREFIELD 
JOIN(SELECT id from idx limit length) as I
WHERE JSON_EXTRACT(QUESTION, CONCAT('$.SubQuestion[',I.id,'].Alias')) = Alias AND REFERENCEID = refVal; 
 
RETURN result;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `GetPercent` (`lacakVal` INT, `maxVal` INT) RETURNS DOUBLE(11,2) BEGIN
DECLARE jml double;
SET jml = (1*100)/(lacakVal*maxVal);
RETURN jml;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_percentage` (`val` DOUBLE) RETURNS DOUBLE(5,2) BEGIN	
RETURN ROUND((val*100),2);
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `jml_lulusan` (`gradDate` DATE, `prodi` INT) RETURNS INT(11) BEGIN
	DECLARE jml INT;
	SELECT COUNT(P1.USERID) INTO jml FROM ALUMNPROFILE P1 WHERE YEAR(P1.GRADUATIONDATE) = YEAR(gradDate) AND P1.PRODID = prodi; 
RETURN jml;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `jml_terlacak` (`gradDate` DATE, `prodi` INT) RETURNS INT(11) BEGIN
	DECLARE JML INT;
        
    SELECT COUNT(DISTINCT A.USERID) INTO JML
		FROM ANSWER AS A		 
		WHERE USERID IN (SELECT USERID FROM ALUMNPROFILE WHERE YEAR(GRADUATIONDATE) = YEAR(gradDate)
        AND PRODID = prodi);	
RETURN JML;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `jml_terlacak_5thn` (`prodi` INT) RETURNS INT(11) BEGIN
	DECLARE jml INT;
	SELECT COUNT(USERID) INTO jml FROM `alumnprofile` WHERE YEAR(GRADUATIONDATE) >= YEAR(CURDATE())-5 AND PRODID = prodi;
RETURN jml;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `jml_terlacak_prod` (`prodi` INT) RETURNS INT(11) BEGIN
	DECLARE JML INT;
	SELECT COUNT(DISTINCT A.USERID) INTO JML 
		FROM ANSWER AS A
		INNER JOIN ALUMNPROFILE AS P ON P.USERID = A.USERID 
		WHERE YEAR(P.GRADUATIONDATE) >= YEAR(CURDATE())-5
        AND P.PRODID = prodi;	
RETURN JML;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `jsonTotalArray` (`refVal` INT) RETURNS INT(11) BEGIN
DECLARE length INT;
	SELECT json_length(question, '$.SubQuestion') INTO length FROM questionairefield WHERE referenceid = refVal;
RETURN length;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `alumnprofile`
--

CREATE TABLE `alumnprofile` (
  `PROFILEID` int(11) NOT NULL,
  `USERID` int(11) NOT NULL,
  `PRODID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `GRADUATIONDATE` datetime NOT NULL,
  `GPA` float NOT NULL,
  `EMAIL` varchar(30) NOT NULL,
  `PHONE` varchar(13) NOT NULL,
  `ADDRESS` varchar(100) NOT NULL,
  `FACEBOOK` varchar(30) DEFAULT NULL,
  `INSTAGRAM` varchar(30) DEFAULT NULL,
  `LINKDIN` varchar(30) DEFAULT NULL,
  `YEARIN` int(11) NOT NULL,
  `DATEOUT` datetime DEFAULT NULL,
  `CREATEDDATE` datetime DEFAULT NULL,
  `CREATEDBY` varchar(30) DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `UPDATEBY` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumnprofile`
--

INSERT INTO `alumnprofile` (`PROFILEID`, `USERID`, `PRODID`, `NAME`, `GRADUATIONDATE`, `GPA`, `EMAIL`, `PHONE`, `ADDRESS`, `FACEBOOK`, `INSTAGRAM`, `LINKDIN`, `YEARIN`, `DATEOUT`, `CREATEDDATE`, `CREATEDBY`, `UPDATEDATE`, `UPDATEBY`) VALUES
(1, 4, 2, 'Alimni1', '2019-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2015, NULL, NULL, NULL, NULL, NULL),
(2, 5, 2, 'Alumni2', '2019-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2015, NULL, NULL, NULL, NULL, NULL),
(3, 6, 2, 'Alumni3', '2019-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2015, NULL, NULL, NULL, NULL, NULL),
(4, 7, 2, 'Alumni4', '2019-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2015, NULL, NULL, NULL, NULL, NULL),
(5, 8, 2, 'Alumni5', '2019-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2015, NULL, NULL, NULL, NULL, NULL),
(6, 9, 3, 'Alumni6', '2019-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2015, NULL, NULL, NULL, NULL, NULL),
(7, 10, 3, 'Alumni7', '2018-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2014, NULL, NULL, NULL, NULL, NULL),
(8, 11, 3, 'Alumni8', '2018-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2014, NULL, NULL, NULL, NULL, NULL),
(9, 12, 3, 'Alumni9', '2018-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2014, NULL, NULL, NULL, NULL, NULL),
(10, 13, 3, 'Alumni10', '2018-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2014, NULL, NULL, NULL, NULL, NULL),
(11, 14, 2, 'Alumni11', '2018-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2014, NULL, NULL, NULL, NULL, NULL),
(12, 15, 2, 'Alumni12', '2018-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2014, NULL, NULL, NULL, NULL, NULL),
(13, 16, 2, 'Alumni13', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(14, 17, 2, 'Alumni14', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(15, 18, 2, 'Alumni15', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(16, 19, 3, 'Alumni16', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(17, 20, 3, 'Alumni17', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(18, 21, 3, 'Alumni18', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(19, 22, 3, 'Alumni19', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(20, 23, 3, 'Alumni20', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(21, 24, 3, 'Alumni21', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(22, 25, 3, 'Alumni22', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(23, 26, 3, 'Alumni23', '2017-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2013, NULL, NULL, NULL, NULL, NULL),
(24, 27, 3, 'Alumni24', '2016-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2012, NULL, NULL, NULL, NULL, NULL),
(25, 28, 3, 'Alumni25', '2016-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2012, NULL, NULL, NULL, NULL, NULL),
(26, 29, 3, 'Alumni26', '2016-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2012, NULL, NULL, NULL, NULL, NULL),
(27, 30, 3, 'Alumni27', '2016-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2012, NULL, NULL, NULL, NULL, NULL),
(28, 31, 3, 'Alumni28', '2016-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2012, NULL, NULL, NULL, NULL, NULL),
(29, 32, 3, 'Alumni29', '2016-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2012, NULL, NULL, NULL, NULL, NULL),
(30, 33, 3, 'Alumni30', '2016-10-23 00:00:00', 3, 'alumni@polban.ac.id', '08932323', 'Bandung', NULL, NULL, NULL, 2012, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `FIELDID` int(11) NOT NULL,
  `USERID` int(11) NOT NULL,
  `ANSWERID` int(11) NOT NULL,
  `ANSWER` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `CREATEDDATE` datetime DEFAULT NULL,
  `CREATEDBY` varchar(30) DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `UPDATEBY` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`FIELDID`, `USERID`, `ANSWERID`, `ANSWER`, `CREATEDDATE`, `CREATEDBY`, `UPDATEDATE`, `UPDATEBY`) VALUES
(1, 4, 1, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 4, 2, 'SEBELUM LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 4, 4, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 4, 5, 'PROGRAMMER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 4, 6, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 4, 7, '[\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 4, 8, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 4, 9, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 4, 10, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 5, 11, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 5, 12, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 5, 14, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 5, 15, 'PROGRAMMER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 5, 16, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 5, 17, '[\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 5, 18, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 5, 19, '[2,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 5, 20, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 6, 26, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 6, 27, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 6, 29, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 6, 30, 'PROGRAMMER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 6, 31, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 6, 32, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 6, 33, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 6, 34, '[3,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 6, 35, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 7, 36, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 7, 37, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 7, 39, 'LOKAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 7, 40, 'PROGRAMMER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 7, 41, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 7, 42, '[\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"DESIGN SOFTWARE APPLICATION\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 7, 43, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 7, 44, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 7, 45, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 8, 57, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 8, 58, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 8, 59, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 8, 60, 'LOKAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 8, 61, 'PROGRAMMER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 8, 62, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 8, 63, '[\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"DESIGN SOFTWARE APPLICATION\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 8, 64, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 8, 65, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 8, 66, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 9, 72, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 9, 73, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 9, 74, '3-6 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 9, 75, 'NASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 9, 76, 'PROGRAMMER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 9, 77, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 9, 78, '[\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"DESIGN SOFTWARE APPLICATION\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 9, 79, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 9, 80, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 9, 81, '[1,1,1,2,3,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 10, 87, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 10, 88, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 10, 89, '> 6 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 10, 90, 'LOKAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 10, 91, 'SENIOR PROGRAMMER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 10, 92, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 10, 93, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 10, 94, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 10, 95, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 10, 96, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 11, 102, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 11, 103, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 11, 104, '> 6 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 11, 105, 'LOKAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 11, 106, 'SENIOR PROGRAMMER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 11, 107, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 11, 108, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"TECHNICAL WRITER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"DESIGN SOFTWARE APPLICATION\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 11, 109, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 11, 110, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 11, 111, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 12, 117, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 12, 118, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 12, 119, '> 6 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 12, 120, 'LOKAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 12, 121, 'DATABASE ADMINISTRATOR', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 12, 122, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 12, 123, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"TECHNICAL WRITER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"DESIGN SOFTWARE APPLICATION\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 12, 124, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 12, 125, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 12, 126, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 13, 132, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 13, 133, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 13, 134, '> 6 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 13, 135, 'LOKAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 13, 136, 'DATABASE ADMINISTRATOR', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 13, 137, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 13, 138, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"TECHNICAL WRITER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"DESIGN SOFTWARE APPLICATION\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 13, 139, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 13, 140, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 13, 141, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 14, 147, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 14, 148, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 14, 149, '> 6 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 14, 150, 'LOKAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 14, 151, 'WEB DEVELOPER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 14, 152, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 14, 153, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 14, 154, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 14, 155, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 14, 156, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 15, 162, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 15, 163, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 15, 164, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 15, 165, 'NASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 15, 166, 'WEB DEVELOPER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 15, 167, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 15, 168, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"SEDANG\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"SEDANG\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 15, 169, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 15, 170, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 15, 171, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 16, 177, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 16, 178, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 16, 179, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 16, 180, 'NASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 16, 181, 'MULTIMEDIA DEVELOPER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 16, 182, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 16, 183, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 16, 184, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 16, 185, '[4,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 16, 186, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 17, 192, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 17, 193, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 17, 194, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 17, 195, 'NASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 17, 196, 'MULTIMEDIA DEVELOPER', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 17, 197, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 17, 198, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 17, 199, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 17, 200, '[4,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 17, 201, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 18, 207, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 18, 208, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 18, 209, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 18, 210, 'NASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 18, 211, 'INFORMATION SYSTEM ANALYST', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 18, 212, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 18, 213, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 18, 214, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 18, 215, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 18, 216, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 19, 222, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 19, 223, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 19, 224, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 19, 225, 'NASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 19, 226, 'INFORMATION SYSTEM ANALYST', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 19, 227, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 19, 228, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"WEB DEVELOPER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 19, 229, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 19, 230, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 19, 231, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 20, 237, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 20, 238, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 20, 239, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 20, 240, 'NASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 20, 241, 'COMPUTER SUPPORT SPECIALIST', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 20, 242, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 20, 243, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 20, 244, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 20, 245, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 20, 246, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 21, 252, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 21, 253, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 21, 254, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 21, 255, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 21, 256, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 21, 257, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 21, 258, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 21, 259, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 21, 260, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 21, 261, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 22, 267, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 22, 268, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 22, 269, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 22, 270, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 22, 271, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 22, 272, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 22, 273, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 22, 274, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 22, 275, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 22, 276, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 23, 282, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 23, 283, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 23, 284, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 23, 285, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 23, 286, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 23, 287, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 23, 288, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 23, 289, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 23, 290, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 23, 291, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 24, 297, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 24, 298, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 24, 299, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 24, 300, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 24, 301, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 24, 302, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 24, 303, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 24, 304, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 24, 305, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 24, 306, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 25, 312, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 25, 313, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 25, 314, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 25, 315, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 25, 316, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 25, 317, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 25, 318, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"LOKAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 25, 319, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 25, 320, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 25, 321, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 26, 327, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 26, 328, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 26, 329, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 26, 330, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 26, 331, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 26, 332, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 26, 333, '[\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"RENDAH\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"RENDAH\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 26, 334, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 26, 335, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 26, 336, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 27, 342, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 27, 343, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 27, 344, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 27, 345, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 27, 346, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 27, 347, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 27, 348, '[\r\n{\r\n    \"YEAR\":2016, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"RENDAH\"    \r\n  },\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"RENDAH\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"RENDAH\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 27, 349, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 27, 350, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 27, 351, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 28, 357, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 28, 358, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 28, 359, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 28, 360, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 28, 361, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 28, 362, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 28, 363, '[\r\n{\r\n    \"YEAR\":2016, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"RENDAH\"    \r\n  },\r\n{\r\n    \"YEAR\":2017, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"RENDAH\"    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"RENDAH\"    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"RENDAH\"    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 28, 364, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 28, 365, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 28, 366, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 29, 372, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 29, 373, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 29, 374, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 29, 375, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 29, 376, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 29, 377, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 29, 378, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 29, 379, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 29, 380, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 29, 381, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 30, 387, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 30, 388, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 30, 389, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 30, 390, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 30, 391, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 30, 392, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 30, 393, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 30, 394, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 30, 395, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 30, 396, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 31, 402, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 31, 403, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 31, 404, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 31, 405, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 31, 406, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 31, 407, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 31, 408, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 31, 409, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 31, 410, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 31, 411, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 32, 417, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 32, 418, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 32, 419, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 32, 420, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 32, 421, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 32, 422, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 32, 423, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 32, 424, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 32, 425, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 32, 426, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(1, 33, 432, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(2, 33, 433, 'SESUDAH LULUS', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(3, 33, 434, '0-3 BULAN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(4, 33, 435, 'INTERNASIONAL', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(5, 33, 436, 'LAIN-LAIN', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(26, 33, 437, 'SUDAH BEKERJA', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(28, 33, 438, '[\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2018, \r\n    \"LEVEL\": \"NASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  },\r\n  {\r\n    \"YEAR\":2019, \r\n    \"LEVEL\": \"INTERNASIONAL\", \r\n    \"POSISI\":\"PROGRAMMER\"\r\n    \r\n  }\r\n]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(40, 33, 439, '[\"WEB PROGRAMMING\",\"SYSTEM ADMINISTRATOR\"]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(56, 33, 440, '[3,3,3,3,3,2,3]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(57, 33, 441, '[3,3,3,4,3,4,3,4]', '2019-10-27 22:00:58', 'WULAN', '2019-10-27 22:00:58', 'WULAN'),
(6, 21, 447, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 22, 448, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 23, 449, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 24, 450, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 25, 451, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 26, 452, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 27, 453, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 28, 454, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 29, 455, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 30, 456, 'SEDANG', NULL, NULL, NULL, NULL),
(6, 31, 457, 'RENDAH', NULL, NULL, NULL, NULL),
(6, 32, 458, 'RENDAH', NULL, NULL, NULL, NULL),
(6, 33, 459, 'RENDAH', NULL, NULL, NULL, NULL),
(3, 34, 465, '0-3 BULAN', NULL, NULL, NULL, NULL),
(3, 5, 474, '> 6 BULAN', '2019-11-18 14:30:26', 'WULAN', '2019-11-18 14:30:26', 'WULAN'),
(3, 6, 475, '> 6 BULAN', '2019-11-18 14:30:26', 'WULAN', '2019-11-18 14:30:26', 'WULAN'),
(3, 7, 476, '3-6 BULAN', '2019-11-18 14:30:26', 'WULAN', '2019-11-18 14:30:26', 'WULAN');

-- --------------------------------------------------------

--
-- Table structure for table `bossprofile`
--

CREATE TABLE `bossprofile` (
  `BOSSID` int(11) NOT NULL,
  `USERID` int(11) DEFAULT NULL,
  `PROFILEID` int(11) DEFAULT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `EMAIL` varchar(30) DEFAULT NULL,
  `PHONE` varchar(13) DEFAULT NULL,
  `COMPANYPHONE` varchar(13) DEFAULT NULL,
  `COMPANYADDRESS` varchar(100) DEFAULT NULL,
  `JOBPOSITION` varchar(100) DEFAULT NULL,
  `CREATEDDATE` datetime DEFAULT NULL,
  `CREATEDBY` varchar(30) DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `UPDATEBY` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `function`
--

CREATE TABLE `function` (
  `functionid` varchar(30) NOT NULL,
  `menuname` varchar(50) NOT NULL,
  `uri` varchar(1000) DEFAULT NULL,
  `icon` varchar(1000) DEFAULT NULL,
  `parentid` varchar(30) DEFAULT NULL,
  `sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `function`
--

INSERT INTO `function` (`functionid`, `menuname`, `uri`, `icon`, `parentid`, `sequence`) VALUES
('MF01', 'Kelola Data User', '/user', NULL, NULL, 11),
('TF01', 'Beranda', '/beranda', NULL, NULL, 1),
('TF02', 'Dasbor', NULL, NULL, NULL, 2),
('TF03', 'Masa Tunggu Lulusan', '/dasbor', NULL, 'TF02', 3),
('TF04', 'Kesesuaian Profil Lulusan', '/dasbor/posisi', NULL, 'TF02', 4),
('TF05', 'Kinerja Lulusan', '/dasbor/level', NULL, 'TF02', 5),
('TF06', 'Kepuasan Pengguna Lulusan', '/dasbor/kepuasan', NULL, 'TF02', 6),
('TF07', 'Trend Metode', '/#', NULL, 'TF02', 7),
('TF08', 'Trend Teknologi', '/#', NULL, 'TF02', 8),
('TF09', 'Standar SDLC', '/#', NULL, 'TF02', 9),
('TF10', 'Mata Kuliah Penting', '/#', NULL, 'TF02', 10);

-- --------------------------------------------------------

--
-- Table structure for table `idx`
--

CREATE TABLE `idx` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `idx`
--

INSERT INTO `idx` (`id`) VALUES
(0),
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22);

-- --------------------------------------------------------

--
-- Table structure for table `major`
--

CREATE TABLE `major` (
  `MAJORID` int(11) NOT NULL,
  `MAJORNAME` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `major`
--

INSERT INTO `major` (`MAJORID`, `MAJORNAME`) VALUES
(1, 'Jurusan Teknik Informatika dan Komputer\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `PRODID` int(11) NOT NULL,
  `MAJORID` int(11) NOT NULL,
  `PRODINAME` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`PRODID`, `MAJORID`, `PRODINAME`) VALUES
(2, 1, 'D3 Teknik Informatika'),
(3, 1, 'D4 Teknik Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `questionairefield`
--

CREATE TABLE `questionairefield` (
  `FIELDID` int(11) NOT NULL,
  `SECTIONID` int(11) DEFAULT NULL,
  `ISREQUIRED` smallint(6) DEFAULT NULL,
  `REFERENCEID` int(11) NOT NULL,
  `QUESTION` text DEFAULT NULL,
  `FIELDTYPE` varchar(15) DEFAULT NULL,
  `CONDITIONALLOGIC` text DEFAULT NULL,
  `CREATEDDATE` datetime DEFAULT NULL,
  `CREATEDBY` varchar(30) DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `UPDATEBY` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionairefield`
--

INSERT INTO `questionairefield` (`FIELDID`, `SECTIONID`, `ISREQUIRED`, `REFERENCEID`, `QUESTION`, `FIELDTYPE`, `CONDITIONALLOGIC`, `CREATEDDATE`, `CREATEDBY`, `UPDATEDATE`, `UPDATEBY`) VALUES
(1, 1, 1, 1, '{\n  \"Question\": \"Apakah saat ini Anda sedang bekerja?\",\n  \"Options\": {\n    \"A1\": {\"Belum Bekerja\",\"BELUM BEKERJA\"},\n    \"A2\": {\"Sudah Bekerja,\"SUDAH BEKERJA\"}\n  }\n}', 'RADIO', '', '2019-10-27 15:42:45', 'WULAN', '2019-10-27 15:42:45', 'WULAN'),
(2, 2, 1, 2, '{  \"Question\": \"Kapankah Anda memperoleh pekerjaan pertama?\",  \"Options\": {    \"A1\": {\"Sebelum Lulus\",\"SEBELUM LULUS\"},    \"A2\": {\"Sesudah Lulus,\"SESUDAH LULUS\"}  }}', 'RADIO', '', '2019-10-27 16:17:18', 'WULAN', '2019-10-27 16:17:18', 'WULAN'),
(3, 11, 1, 3, '{  \"Question\": \"Berapa bulan setelah kelulusan Anda memperoleh pekerjaan pertama?\",  \"Options\": {    \"A1\": {\"0-3 Bulan\",\"0-3 BULAN\"},    \"A2\": {\"3-6 Bulan\",\"3-6 BULAN\"},    \"A3\": {\"> 6 Bulan\",\"> 6 BULAN\"}  }}', 'RADIO', '', '2019-10-27 16:17:18', 'WULAN', '2019-10-27 16:17:18', 'WULAN'),
(4, 12, 1, 4, '{\n  \"Question\": \"Apa jenis perusahaan/instansi tempat Anda bekerja saat ini?\",\n  \"Options\": {\n    \"A1\": {\"Perusahaan lokal/wilayah/berwirausaha tidak berbadan hukum\",\"LOKAL\"},\n    \"A2\": {\"Perusahaan nasional/berwirausaha berbadan hukum\",\"NASIONAL\"},\n	\"A2\": {\"Multinasional/international\",\"INTERNASIONAL\"}\n  }\n}', 'RADIO', '', '2019-10-27 16:30:54', 'WULAN', '2019-10-27 16:30:54', 'WULAN'),
(5, 12, 1, 5, '{\"Question\": \"Manakah profil lulusan di bawah ini yang sesuai dengan  pekerjaan Anda di perusahaan tempat Anda bekerja saat ini. Anda boleh memilih lebih dari satu jawaban.\",   \"Options\": [   {\"Name\":\"Programmer\",\"Value\":\"PROGRAMMER\"},   {\"Name\":\"Software Application Tester\",\"Value\":\"SOFTWARE APPLICATION TESTER\"},   {\"Name\":\"Technical Writer\",\"Value\":\"TECHNICAL WRITER\"},   {\"Name\":\"Senior Programmer\",\"Value\":\"SENIOR PROGRAMMER\"},   {\"Name\":\"Database Administrator\",\"Value\":\"DATABASE ADMINISTRATOR\"},   {\"Name\":\"Web Developer\",\"Value\":\"WEB DEVELOPER\"},   {\"Name\":\"Multimedia Developer\",\"Value\":\"MULTIMEDIA DEVELOPER\"},   {\"Name\":\"Information System Analyst\",\"Value\":\"INFORMATION SYSTEM ANALYSIT\"},   {\"Name\":\"Computer Support Specialist\",\"Value\":\"COMPUTER SUPPORT SPECIALIST\"},   {\"Name\":\"Lainnya\",\"Value\":\"LAIN-LAIN\"} ]} ', 'CHECKBOX', '', '2019-10-27 16:30:54', 'WULAN', '2019-10-27 16:30:54', 'WULAN'),
(6, 12, 1, 6, '{  \"Question\": \"Jika pekerjaan Anda dan profil lulusan tidak ada yang sesuai, yaitu pilihan a s.d. j pada pertanyaan sebelumnya, apakah pekerjaan Anda saat ini masih berhubungan dengan bidang Informatika?\",  \"Options\": [    {      \"Name\": \"Ya\",      \"Value\": \"SEDANG\"    },    {      \"Name\": \"Tidak\",      \"Value\": \"RENDAH\"    }  ]}', 'RADIO', '', '2019-10-27 16:37:45', 'WULAN', '2019-10-27 16:37:45', 'WULAN'),
(7, 3, 1, 7, '{  \"Question\": \"Metode pengembangan perangkat lunak apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini?\",  \"Options\": {    \"A1\": {\"Scrum\",\"SCRUM\"},    \"A2\": {\"Agile\",\"AGILE\"},    \"A3\": {\"Waterfall\",\"WATERFALL\"},    \"A4\": {\"Incremental\",\"INCREMENTAL\"},    \"A5\": {\"Prototyping\",\"PROTOTYPING\"},    \"A6\": {\"Spiral\",\"SPIRAL\"},    \"A11\": {\"Lainnya\",\"LAIN-LAIN\"}  }}', 'RADIO', '', '2019-10-27 16:37:46', 'WULAN', '2019-10-27 16:37:46', 'WULAN'),
(8, 3, 1, 8, '{\n  \"Question\": \"Metode pengujian software apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini? Anda boleh memilih lebih dari satu jawaban.\",\n  \"Options\": {\n    \"A1\": {\"Black Box\",\"BLACK BOX\"},\n    \"A2\": {\"White Box\",\"WHITE BOX\"},\n    \"A3\": {\"Manual Testing\",\"MANUAL TESTING\"},\n    \"A4\": {\"Automation\",\"AUTOMATION\"},\n    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}\n  }\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(9, 3, 1, 9, '{\n  \"Question\": \"Alat pengembangan proyek apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini? Anda boleh memilih lebih dari satu jawaban.\",\n  \"Options\": {\n    \"A1\": {\"Trello\",\"TRELLO\"},\n    \"A2\": {\"Spreadsheet\",\"SPREADSHEET\"},\n    \"A3\": {\"Workbook\",\"WORKBOOK\"},\n    \"A4\": {\"Jira\",\"JIRA\"},\n	\"A5\": {\"Granchart\",\"GRANCHART\"},\n    \"A6\": {\"Lainnya\",\"LAIN-LAIN\"}\n  }\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(10, 3, 1, 10, '{\n  \"Question\": \"Tool modelling apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini? Anda boleh memilih lebih dari satu jawaban.\",\n  \"Options\": {\n    \"A1\": {\"UML\",\"UML\"},\n    \"A2\": {\"DFD\",\"DFD\"},\n    \"A3\": {\"Flowchart\",\"FLOWCHART\"},\n    \"A4\": {\"Entity Relational Diagram\",\"ERD\"},\n    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}\n  }\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(11, 3, 1, 11, '{\n  \"Question\": \"Bahasa pemrograman apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini pada saat pengembangan perangkat lunak? Anda boleh memilih lebih dari satu jawaban.\",\n  \"Options\": {\n    \"A1\": {\"C\",\"C\"},\n    \"A2\": {\"C#\",\"C#\"},\n    \"A3\": {\"C++\",\"C++\"},\n    \"A4\": {\"Java\",\"JAVA\"},\n    \"A5\": {\"PHP\",\"PHP\"},\n    \"A6\": {\"Python\",\"PYTHON\"},\n    \"A7\": {\"Javascript\",\"JAVASCRIPT\"},\n    \"A8\": {\"Lainnya\",\"LAIN-LAIN\"}\n  }\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(12, 3, 1, 12, '{\n  \"Question\": \"Jika saat ini Anda sedang bekerja, framework development apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini pada saat pengembangan perangkat lunak? Anda boleh memilih lebih dari satu jawaban.\",\n  \"Options\": {\n    \"A1\": {\"Spring\",\"C\"},\n    \"A2\": {\"Laravel\",\"C#\"},\n    \"A3\": {\".NET\",\"C++\"},\n    \"A4\": {\"Hibernate\",\"JAVA\"},\n    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}\n  }\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(13, 3, 1, 13, '{\n  \"Question\": \"Teknologi basis data apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini pada saat pengembangan software?\",\n  \"Options\": {\n    \"A1\": {\"MySQL\",\"MYSQL\"},\n    \"A2\": {\"SQL Server\",\"SQL SERVER\"},\n    \"A3\": {\"Oracle\",\"ORACLE\"},\n    \"A4\": {\"MongoDB\",\"MONGODB\"},\n    \"A5\": {\"NoSQL\",\"NOSQL\"},\n    \"A6\": {\"Lainnya\",\"LAIN-LAIN\"}\n  }\n}', 'RADIO', '', '2019-10-27 17:08:08', 'WULAN', '2019-10-27 17:08:08', 'WULAN'),
(14, 3, 1, 14, '{\n  \"Question\": \"Alat pengujian perangkat lunak apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini? Anda boleh memilih lebih dari satu jawaban\",\n  \"Options\": {\n    \"A1\": {\"Selenium\",\"SELENIUM\"},\n    \"A2\": {\"Smartbear\",\"SMARTBEAR\"},\n    \"A3\": {\"AWS Device Farm\",\"AWS\"},\n    \"A4\": {\"Appium\",\"APPIUM\"},\n    \"A5\": {\"Katalon\",\"KATALON\"},\n    \"A6\": {\"JMeter\",\"JMETER\"},\n    \"A7\": {\"Lainnya\",\"LAIN-LAIN\"}\n  }\n}', 'RADIO', '', '2019-10-27 17:08:08', 'WULAN', '2019-10-27 17:08:08', 'WULAN'),
(15, 3, 1, 15, '{  \"Question\": \"Alat collaborative development apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini  pada saat melakukan implementasi? Anda boleh memilih lebih dari satu jawaban.\",  \"Options\": {    \"A1\": {\"Gitlab\",\"GITLAB\"},    \"A2\": {\"Github\",\"GITHUB\"},    \"A3\": {\"Bitbucket\",\"BITBUCKET\"},    \"A4\": {\"SVN\",\"SVN\"},    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}  }}', 'CHECKBOX', '', '2019-10-27 17:08:08', 'WULAN', '2019-10-27 17:08:08', 'WULAN'),
(16, 3, 1, 16, '{\n  \"Question\": \"Jika saat ini anda sedang bekerja, standar apa yang digunakan dalam pengembangan perangkat lunak?\",\n  \"Options\": {\n    \"A1\": {\"Standar International/IEEE/ISO\",\"STANDAR INTERNASIONAL\"},\n    \"A2\": {\"Standar dari komunitas software engineering\",\"STANDAR KOMUNITAS\"},\n    \"A3\": {\"Lokal/standar yang dibuat oleh perusahaan\",\"STANDAR LOKAL\"},\n    \"A4\": {\"Tidak menggunakan standar\",\"TIDAK ADA STANDAR\"},\n    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}\n  }\n}', 'RADIO', '', '2019-10-27 17:08:08', 'WULAN', '2019-10-27 17:08:08', 'WULAN'),
(17, 3, 1, 17, '{  \"Question\": \"Berilah tanda ceklis pada pernyataan konten mata kuliah dibawah ini yang paling dibutuhkan dengan bidang pekerjaan Anda saat ini.\",  \"SubQuestion\": {	{		\"Question\":\"Pemrograman Berorientasi Objek\",		\"Options\":{			\"A1\": {\"Sangat dibutuhkan\",\"4\"},			\"A2\": {\"Dibutuhkan\",\"3\"},            \"A3\": {\"Cukup dibutuhkan\",\"2\"},            \"A4\": {\"Tidak dibutuhkan\",\"1\"}		},        \"Type\":\"RADIO\"	},    	{		\"Question\":\"Proyek 1\",		\"Options\":{			\"A1\": {\"Sangat dibutuhkan\",\"4\"},			\"A2\": {\"Dibutuhkan\",\"3\"},            \"A3\": {\"Cukup dibutuhkan\",\"2\"},            \"A4\": {\"Tidak dibutuhkan\",\"1\"}		},        \"Type\":\"RADIO\"	},    	{		\"Question\":\"Proyek 1\",		\"Options\":{			\"A1\": {\"Sangat dibutuhkan\",\"4\"},			\"A2\": {\"Dibutuhkan\",\"3\"},            \"A3\": {\"Cukup dibutuhkan\",\"2\"},            \"A4\": {\"Tidak dibutuhkan\",\"1\"}		},        \"Type\":\"RADIO\"	},    	{		\"Question\":\"Ilmu Komputer\",		\"Options\":{			\"A1\": {\"Sangat dibutuhkan\",\"4\"},			\"A2\": {\"Dibutuhkan\",\"3\"},            \"A3\": {\"Cukup dibutuhkan\",\"2\"},            \"A4\": {\"Tidak dibutuhkan\",\"1\"}		},        \"Type\":\"RADIO\"	},    	{		\"Question\":\"Sistem Basis Data\",		\"Options\":{			\"A1\": {\"Sangat dibutuhkan\",\"4\"},			\"A2\": {\"Dibutuhkan\",\"3\"},            \"A3\": {\"Cukup dibutuhkan\",\"2\"},            \"A4\": {\"Tidak dibutuhkan\",\"1\"}		},        \"Type\":\"RADIO\"	}      }}', 'MULTIQUESTION', '', '2019-10-27 19:24:52', 'WULAN', '2019-10-27 19:24:52', 'WULAN'),
(18, 4, 0, 18, '{\n  \"Question\": \"Apa nama perusahaan tempat Anda bekerja saat ini.\",\n}', 'VARCHAR', '', '2019-10-27 19:24:52', 'WULAN', '2019-10-27 19:24:52', 'WULAN'),
(19, 4, 0, 19, '{\n  \"Question\": \"Dimana alamat nama perusahaan tempat Anda bekerja saat ini? (alamat lengkap dengan nama kota, provinsi dan kode pos).\",\n}', 'LONGVARCHAR', '', '2019-10-27 19:24:52', 'WULAN', '2019-10-27 19:24:52', 'WULAN'),
(20, 4, 0, 20, '{\n  \"Question\": \"Nomor telepon perusahaan tempat Anda bekerja saat ini?\",\n}', 'PHONE', '', '2019-10-27 19:24:52', 'WULAN', '2019-10-27 19:24:52', 'WULAN'),
(21, 4, 0, 21, '{\n  \"Question\": \"Apa email perusahaan tempat Anda bekerja saat ini?\",\n}', 'EMAIL', '', '2019-10-27 19:24:52', 'WULAN', '2019-10-27 19:24:52', 'WULAN'),
(22, 4, 0, 22, '{\n  \"Question\": \"Siapa atasan langsung Anda di perusahaan tempat Anda bekerja saat ini?\",\n}', 'VARCHAR', '', '2019-10-27 19:24:52', 'WULAN', '2019-10-27 19:24:52', 'WULAN'),
(23, 4, 0, 23, '{\n  \"Question\": \"Apa jabatan atasan langsung Anda di perusahaan tempat Anda bekerja saat ini?\",\n}', 'VARCHAR', '', '2019-10-27 19:24:52', 'WULAN', '2019-10-27 19:24:52', 'WULAN'),
(24, 4, 0, 24, '{\n  \"Question\": \"Apa nomor telepon atasan langsung di perusahaan tempat Anda bekerja saat ini?\",\n}', 'PHONE', '', '2019-10-27 19:24:52', 'WULAN', '2019-10-27 19:24:52', 'WULAN'),
(25, 4, 0, 25, '{\n  \"Question\": \"Apa email atasan langsung di perusahaan tempat Anda bekerja saat ini?\",\n}', 'EMAIL', '', '2019-10-27 19:24:52', 'WULAN', '2019-10-27 19:24:52', 'WULAN'),
(26, 5, 1, 1, '{\r\n  \"Question\": \"Apakah saat ini Anda sedang bekerja?\",\r\n  \"Options\": {\r\n    \"A1\": {\"Belum Bekerja\",\"BELUM BEKERJA\"},\r\n    \"A2\": {\"Sudah Bekerja,\"SUDAH BEKERJA\"}\r\n  }\r\n}', 'RADIO', '', '2019-10-27 15:42:45', 'WULAN', '2019-10-27 15:42:45', 'WULAN'),
(27, 6, 1, 26, '{\n  \"Question\": \"Mengapa Anda tidak bekerja?\",\n}', 'LONGVARCHAR', '', '2019-10-27 19:31:47', 'WULAN', '2019-10-27 19:31:47', 'WULAN'),
(28, 7, 1, 27, '{\r\n  \"Question\": \"Sebutkan pekerjaan Anda setelah lulus dari Polban.\",\r\n  \"SubQuestion\": [\r\n    {\r\n      \"Question\": \"Tahun\",\r\n      \"Type\": \"INTEGER\",\r\n      \"Alias\": \"TAHUN\"\r\n    },\r\n    {\r\n      \"Question\": \"Nama Perusahaan\",\r\n      \"Type\": \"VARCHAR\",\r\n      \"Alias\": \"PERUSAHAAN\"\r\n    },\r\n    {\r\n      \"Question\": \"Level Perusahaan\",\r\n      \"Type\": \"RADIO\",\r\n      \"Alias\": \"LEVEL\",\r\n      \"Options\": [\r\n        {\r\n          \"Text\": \"Perusahaan Lokal/Wilayah/Berwirausaha tidak berbadan hukum\",\r\n          \"Value\": \"LOKAL\"\r\n        },\r\n        {\r\n          \"Text\": \"Perusahaan Nasional/Berwirausaha berbadan hukum\",\r\n          \"Value\": \"NASIONAL\"\r\n        },\r\n        {\r\n          \"Text\": \"Multinasional/Internasional\",\r\n          \"Value\": \"INTERNASIONAL\"\r\n        }\r\n      ]\r\n    },\r\n    {\r\n      \"Question\": \"Posisi Pekerjaan\",\r\n      \"Type\": \"RADIO\",\r\n      \"Alias\": \"POSISI\",\r\n      \"Options\": [\r\n        {\r\n          \"Text\": \"Programmer\",\r\n          \"Value\": \"PROGRAMMER\"\r\n        },\r\n        {\r\n          \"Text\": \"Software Application Tester\",\r\n          \"Value\": \"TESTER\"\r\n        },\r\n        {\r\n          \"Text\": \"Technical Writer\",\r\n          \"Value\": \"TECHNICAL WRITER\"\r\n        },\r\n        {\r\n          \"Text\": \"Software Application Designer\",\r\n          \"Value\": \"DESIGN SOFTWARE APPLICATION\"\r\n        },\r\n        {\r\n          \"Text\": \"Senior Programmer\",\r\n          \"Value\": \"SENIOR PROGRAMMER\"\r\n        },\r\n        {\r\n          \"Text\": \"Database Administrator\",\r\n          \"Value\": \"DBA\"\r\n        },\r\n        {\r\n          \"Text\": \"Web Developer\",\r\n          \"Value\": \"WEB DEVELOPER\"\r\n        },\r\n        {\r\n          \"Text\": \"Multimedia Developer\",\r\n          \"Value\": \"MULTIMEDIA DEVELOPER\"\r\n        },\r\n        {\r\n          \"Text\": \"Information System Analyst\",\r\n          \"Value\": \"IS ANALYST\"\r\n        },\r\n        {\r\n          \"Text\": \"Computer Support Specialist\",\r\n          \"Value\": \"IT SUPPORT\"\r\n        },\r\n        {\r\n          \"Text\": \"Lain-lain dan masih berhubungan dengan bidang Informatika\",\r\n          \"Value\": \"SEDANG\"\r\n        },\r\n        {\r\n          \"Text\": \"Lain-lain tetapi tidak berhubungan dengan bidang Informatika\",\r\n          \"Value\": \"RENDAH\"\r\n        }\r\n      ]\r\n    }\r\n  ]\r\n}', 'MULTIQUESTION', '', '2019-10-27 19:41:29', 'WULAN', '2019-10-27 19:41:29', 'WULAN'),
(29, 8, 1, 7, '{  \"Question\": \"Metode pengembangan perangkat lunak apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini?\",  \"Options\": {    \"A1\": {\"Scrum\",\"SCRUM\"},    \"A2\": {\"Agile\",\"AGILE\"},    \"A3\": {\"Waterfall\",\"WATERFALL\"},    \"A4\": {\"Incremental\",\"INCREMENTAL\"},    \"A5\": {\"Prototyping\",\"PROTOTYPING\"},    \"A6\": {\"Spiral\",\"SPIRAL\"},    \"A11\": {\"Lainnya\",\"LAIN-LAIN\"}  }}', 'RADIO', '', '2019-10-27 16:37:46', 'WULAN', '2019-10-27 16:37:46', 'WULAN'),
(30, 8, 1, 8, '{\r\n  \"Question\": \"Metode pengujian software apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini? Anda boleh memilih lebih dari satu jawaban.\",\r\n  \"Options\": {\r\n    \"A1\": {\"Black Box\",\"BLACK BOX\"},\r\n    \"A2\": {\"White Box\",\"WHITE BOX\"},\r\n    \"A3\": {\"Manual Testing\",\"MANUAL TESTING\"},\r\n    \"A4\": {\"Automation\",\"AUTOMATION\"},\r\n    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}\r\n  }\r\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(31, 8, 1, 9, '{\r\n  \"Question\": \"Alat pengembangan proyek apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini? Anda boleh memilih lebih dari satu jawaban.\",\r\n  \"Options\": {\r\n    \"A1\": {\"Trello\",\"TRELLO\"},\r\n    \"A2\": {\"Spreadsheet\",\"SPREADSHEET\"},\r\n    \"A3\": {\"Workbook\",\"WORKBOOK\"},\r\n    \"A4\": {\"Jira\",\"JIRA\"},\r\n	\"A5\": {\"Granchart\",\"GRANCHART\"},\r\n    \"A6\": {\"Lainnya\",\"LAIN-LAIN\"}\r\n  }\r\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(33, 8, 1, 10, '{\r\n  \"Question\": \"Tool modelling apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini? Anda boleh memilih lebih dari satu jawaban.\",\r\n  \"Options\": {\r\n    \"A1\": {\"UML\",\"UML\"},\r\n    \"A2\": {\"DFD\",\"DFD\"},\r\n    \"A3\": {\"Flowchart\",\"FLOWCHART\"},\r\n    \"A4\": {\"Entity Relational Diagram\",\"ERD\"},\r\n    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}\r\n  }\r\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(34, 8, 1, 11, '{\r\n  \"Question\": \"Bahasa pemrograman apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini pada saat pengembangan perangkat lunak? Anda boleh memilih lebih dari satu jawaban.\",\r\n  \"Options\": {\r\n    \"A1\": {\"C\",\"C\"},\r\n    \"A2\": {\"C#\",\"C#\"},\r\n    \"A3\": {\"C++\",\"C++\"},\r\n    \"A4\": {\"Java\",\"JAVA\"},\r\n    \"A5\": {\"PHP\",\"PHP\"},\r\n    \"A6\": {\"Python\",\"PYTHON\"},\r\n    \"A7\": {\"Javascript\",\"JAVASCRIPT\"},\r\n    \"A8\": {\"Lainnya\",\"LAIN-LAIN\"}\r\n  }\r\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(35, 8, 1, 12, '{\r\n  \"Question\": \"Jika saat ini Anda sedang bekerja, framework development apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini pada saat pengembangan perangkat lunak? Anda boleh memilih lebih dari satu jawaban.\",\r\n  \"Options\": {\r\n    \"A1\": {\"Spring\",\"C\"},\r\n    \"A2\": {\"Laravel\",\"C#\"},\r\n    \"A3\": {\".NET\",\"C++\"},\r\n    \"A4\": {\"Hibernate\",\"JAVA\"},\r\n    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}\r\n  }\r\n}', 'CHECKBOX', '', '2019-10-27 16:55:02', 'WULAN', '2019-10-27 16:55:02', 'WULAN'),
(36, 8, 1, 13, '{\r\n  \"Question\": \"Teknologi basis data apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini pada saat pengembangan software?\",\r\n  \"Options\": {\r\n    \"A1\": {\"MySQL\",\"MYSQL\"},\r\n    \"A2\": {\"SQL Server\",\"SQL SERVER\"},\r\n    \"A3\": {\"Oracle\",\"ORACLE\"},\r\n    \"A4\": {\"MongoDB\",\"MONGODB\"},\r\n    \"A5\": {\"NoSQL\",\"NOSQL\"},\r\n    \"A6\": {\"Lainnya\",\"LAIN-LAIN\"}\r\n  }\r\n}', 'RADIO', '', '2019-10-27 17:08:08', 'WULAN', '2019-10-27 17:08:08', 'WULAN'),
(37, 8, 1, 14, '{\r\n  \"Question\": \"Alat pengujian perangkat lunak apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini? Anda boleh memilih lebih dari satu jawaban\",\r\n  \"Options\": {\r\n    \"A1\": {\"Selenium\",\"SELENIUM\"},\r\n    \"A2\": {\"Smartbear\",\"SMARTBEAR\"},\r\n    \"A3\": {\"AWS Device Farm\",\"AWS\"},\r\n    \"A4\": {\"Appium\",\"APPIUM\"},\r\n    \"A5\": {\"Katalon\",\"KATALON\"},\r\n    \"A6\": {\"JMeter\",\"JMETER\"},\r\n    \"A7\": {\"Lainnya\",\"LAIN-LAIN\"}\r\n  }\r\n}', 'RADIO', '', '2019-10-27 17:08:08', 'WULAN', '2019-10-27 17:08:08', 'WULAN'),
(38, 8, 1, 15, '{\r\n  \"Question\": \"Alat collaborative development apa yang saat ini digunakan oleh perusahaan tempat Anda bekerja saat ini  pada saat melakukan implementasi? Anda boleh memilih lebih dari satu jawaban.\",\r\n  \"Options\": {\r\n    \"A1\": {\"Gitlab\",\"GITLAB\"},\r\n    \"A2\": {\"Github\",\"GITHUB\"},\r\n    \"A3\": {\"Bitbucket\",\"BITBUCKET\"},\r\n    \"A4\": {\"SVN\",\"SVN\"},\r\n    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}\r\n  }\r\n}', 'CHECKBOX', '', '2019-10-27 17:08:08', 'WULAN', '2019-10-27 17:08:08', 'WULAN'),
(39, 8, 1, 16, '{\r\n  \"Question\": \"Jika saat ini anda sedang bekerja, standar apa yang digunakan dalam pengembangan perangkat lunak?\",\r\n  \"Options\": {\r\n    \"A1\": {\"Standar International/IEEE/ISO\",\"STANDAR INTERNASIONAL\"},\r\n    \"A2\": {\"Standar dari komunitas software engineering\",\"STANDAR KOMUNITAS\"},\r\n    \"A3\": {\"Lokal/standar yang dibuat oleh perusahaan\",\"STANDAR LOKAL\"},\r\n    \"A4\": {\"Tidak menggunakan standar\",\"TIDAK ADA STANDAR\"},\r\n    \"A5\": {\"Lainnya\",\"LAIN-LAIN\"}\r\n  }\r\n}', 'RADIO', '', '2019-10-27 17:08:08', 'WULAN', '2019-10-27 17:08:08', 'WULAN'),
(40, 8, 1, 28, '{\n  \"Question\": \"Pelatihan apa saja yang pernah Anda ikuti terkait dalam bidang Informatika? Anda boleh memilih lebih dari satu jawaban.\",\n  \"Options\": {\n	\"A1\": {\"Database\",\"DATABASE\"},\n    \"A2\": {\"Web design\",\"WEB DESIGN\"},\n    \"A3\": {\"Desktop Programming\",\"WEB PROGRAMMING\"},\n    \"A4\": {\"System Administration\",\"SYSTEM ADMINISTRATION\"},\n    \"A5\": {\"Web programming\",\"WEB PROGRAMMING\"},\n    \"A6\": {\"Network Administration\",\"NETWORK ADMINISTRATION\"},\n    \"A7\": {\"Graphic design\",\"GRAPHIC DESIGN\"},\n    \"A8\": {\"Office application\",\"OFFICE APPLICATION\"},\n    \"A9\": {\"Cyber Security\",\"CYBER SECURITY\"},\n    \"A10\": {\"Security Analyst\",\"SECURITY ANALYST\"},\n    \"A11\": {\"Mobile programming\",\"MOBILE PROGRAMMING\"},\n    \"A12\": {\"Animation\",\"ANIMATION\"},\n    \"A13\": {\"Game programming\",\"GAME PROGRAMMING\"},\n    \"A14\": {\"Augmented Reality\",\"AUGMENTED REALITY\"},\n    \"A15\": {\"Big Data\",\"BIG DATA\"},\n    \"A16\": {\"Lainnya\",\"LAIN-LAIN\"},\n  }\n}', 'CHECKBOX', '', '2019-10-27 19:54:39', 'WULAN', '2019-10-27 19:54:39', 'WULAN'),
(41, 9, 0, 18, '{\n  \"Question\": \"Apa nama perusahaan tempat Anda bekerja saat ini.\",\n}', 'VARCHAR', NULL, '2019-10-27 19:59:30', 'WULAN', '2019-10-27 19:59:40', 'WULAN'),
(42, 9, 0, 19, '{\n  \"Question\": \"Dimana alamat nama perusahaan tempat Anda bekerja saat ini? (alamat lengkap dengan nama kota, provinsi dan kode pos).\",\n}', 'LONGVARCHAR', NULL, '2019-10-27 19:59:30', 'WULAN', '2019-10-27 19:59:40', 'WULAN'),
(43, 9, 0, 20, '{\n  \"Question\": \"Nomor telepon perusahaan tempat Anda bekerja saat ini?\",\n}', 'PHONE', NULL, '2019-10-27 19:59:30', 'WULAN', '2019-10-27 19:59:40', 'WULAN'),
(44, 9, 0, 21, '{\n  \"Question\": \"Apa email perusahaan tempat Anda bekerja saat ini?\",\n}', 'EMAIL', NULL, '2019-10-27 19:59:30', 'WULAN', '2019-10-27 19:59:40', 'WULAN'),
(45, 9, 0, 22, '{\n  \"Question\": \"Siapa atasan langsung Anda di perusahaan tempat Anda bekerja saat ini?\",\n}', 'VARCHAR', NULL, '2019-10-27 19:59:30', 'WULAN', '2019-10-27 19:59:40', 'WULAN'),
(46, 9, 0, 23, '{\n  \"Question\": \"Apa jabatan atasan langsung Anda di perusahaan tempat Anda bekerja saat ini?\",\n}', 'VARCHAR', NULL, '2019-10-27 19:59:30', 'WULAN', '2019-10-27 19:59:40', 'WULAN'),
(47, 9, 0, 24, '{\n  \"Question\": \"Apa nomor telepon atasan langsung di perusahaan tempat Anda bekerja saat ini?\",\n}', 'PHONE', NULL, '2019-10-27 19:59:30', 'WULAN', '2019-10-27 19:59:40', 'WULAN'),
(48, 9, 0, 25, '{\n  \"Question\": \"Apa email atasan langsung di perusahaan tempat Anda bekerja saat ini?\",\n}', 'EMAIL', NULL, '2019-10-27 19:59:30', 'WULAN', '2019-10-27 19:59:40', 'WULAN'),
(56, 10, 1, 29, '{\"Question\":\"Berilah tanda ceklis pada pernyataan tingkat kepuasan kemampuan umum dibawah ini yang dimiliki oleh alumni jurusan teknik komputer Polban bekerja di perusahaan Anda\",\"SubQuestion\":[{\"Question\":\"Etika\",\"Options\":[{\"A1\":{\"Sangat baik\":\"4\"},\"A2\":{\"Baik\":\"3\"},\"A3\":{\"Cukup Baik\":\"2\"},\"A4\":{\"Tidak Baik\":\"1\"}}],\"Type\":\"RADIO\"},{\"Question\":\"Kompetensi Bidang\",\"Options\":[{\"A1\":{\"Sangat baik\":\"4\"},\"A2\":{\"Baik\":\"3\"},\"A3\":{\"Cukup Baik\":\"2\"},\"A4\":{\"Tidak Baik\":\"1\"}}],\"Type\":\"RADIO\"},{\"Question\":\"Kemampuan berbahasa asing\",\"Options\":[{\"A1\":{\"Sangat baik\":\"4\"},\"A2\":{\"Baik\":\"3\"},\"A3\":{\"Cukup Baik\":\"2\"},\"A4\":{\"Tidak Baik\":\"1\"}}],\"Type\":\"RADIO\"},{\"Question\":\"Penggunaan teknologi informasi\",\"Options\":[{\"A1\":{\"Sangat baik\":\"4\"},\"A2\":{\"Baik\":\"3\"},\"A3\":{\"Cukup Baik\":\"2\"},\"A4\":{\"Tidak Baik\":\"1\"}}],\"Type\":\"RADIO\"},{\"Question\":\"Kemampuan berkomunikasi\",\"Options\":[{\"A1\":{\"Sangat baik\":\"4\"},\"A2\":{\"Baik\":\"3\"},\"A3\":{\"Cukup Baik\":\"2\"},\"A4\":{\"Tidak Baik\":\"1\"}}],\"Type\":\"RADIO\"},{\"Question\":\"Kerjasama\",\"Options\":[{\"A1\":{\"Sangat baik\":\"4\"},\"A2\":{\"Baik\":\"3\"},\"A3\":{\"Cukup Baik\":\"2\"},\"A4\":{\"Tidak Baik\":\"1\"}}],\"Type\":\"RADIO\"},{\"Question\":\"Pengembangan Diri\",\"Options\":[{\"A1\":{\"Sangat baik\":\"4\"},\"A2\":{\"Baik\":\"3\"},\"A3\":{\"Cukup Baik\":\"2\"},\"A4\":{\"Tidak Baik\":\"1\"}}],\"Type\":\"RADIO\"}]}', 'CHECKBOX', '', '2019-10-27 20:08:15', 'WULAN', '2019-10-27 20:08:15', 'WULAN'),
(57, 10, 1, 30, '{\"Question\": \"Berilah tanda ceklis pada pernyataan tingkat kepuasan kompetensi lulusan di bawah ini yang\ndimiliki oleh alumni jurusan Teknik Komputer dan Informatika Polban bekerja di perusahaan\nAnda.\",\n	\"SubQuestion\": {\n		{\n			\"Question\":\"Kemampuan programming\",\n			\"Options\":{\n				\"A1\": {\"Sangat baik\",\"4\"},			\n                \"A2\": {\"Baik\",\"3\"},\n                \"A3\": {\"Cukup Baik\",\"2\"},\n                \"A4\": {\"Tidak Baik\",\"1\"}\n			},        \n            \"Type\":\"RADIO\"	\n		}, \n        {\n			\"Question\":\"Manajemen proyek perangkat lunak\",\n			\"Options\":{\n				\"A1\": {\"Sangat baik\",\"4\"},			\n                \"A2\": {\"Baik\",\"3\"},\n                \"A3\": {\"Cukup Baik\",\"2\"},\n                \"A4\": {\"Tidak Baik\",\"1\"}\n			},        \n            \"Type\":\"RADIO\"	\n		}, \n        {\n			\"Question\":\"Mudah beradaptasi dengan lingkungan baru\",\n			\"Options\":{\n				\"A1\": {\"Sangat baik\",\"4\"},			\n                \"A2\": {\"Baik\",\"3\"},\n                \"A3\": {\"Cukup Baik\",\"2\"},\n                \"A4\": {\"Tidak Baik\",\"1\"}\n			},        \n            \"Type\":\"RADIO\"	\n		}, \n        {\n			\"Question\":\"Kemampuan melakukan presentasi\",\n			\"Options\":{\n				\"A1\": {\"Sangat baik\",\"4\"},			\n                \"A2\": {\"Baik\",\"3\"},\n                \"A3\": {\"Cukup Baik\",\"2\"},\n                \"A4\": {\"Tidak Baik\",\"1\"}\n			},        \n            \"Type\":\"RADIO\"	\n		}, \n        {\n			\"Question\":\"Kepercayaan diri\",\n			\"Options\":{\n				\"A1\": {\"Sangat baik\",\"4\"},			\n                \"A2\": {\"Baik\",\"3\"},\n                \"A3\": {\"Cukup Baik\",\"2\"},\n                \"A4\": {\"Tidak Baik\",\"1\"}\n			},        \n            \"Type\":\"RADIO\"	\n		}, \n        {\n			\"Question\":\"Kepemimpinan\",\n			\"Options\":{\n				\"A1\": {\"Sangat baik\",\"4\"},			\n                \"A2\": {\"Baik\",\"3\"},\n                \"A3\": {\"Cukup Baik\",\"2\"},\n                \"A4\": {\"Tidak Baik\",\"1\"}\n			},        \n            \"Type\":\"RADIO\"	\n		}, \n        {\n			\"Question\":\"Manajemen waktu\",\n			\"Options\":{\n				\"A1\": {\"Sangat baik\",\"4\"},			\n                \"A2\": {\"Baik\",\"3\"},\n                \"A3\": {\"Cukup Baik\",\"2\"},\n                \"A4\": {\"Tidak Baik\",\"1\"}\n			},        \n            \"Type\":\"RADIO\"	\n		}, \n        {\n			\"Question\":\"Ketegasan\",\n			\"Options\":{\n				\"A1\": {\"Sangat baik\",\"4\"},			\n                \"A2\": {\"Baik\",\"3\"},\n                \"A3\": {\"Cukup Baik\",\"2\"},\n                \"A4\": {\"Tidak Baik\",\"1\"}\n			},        \n            \"Type\":\"RADIO\"	\n		}, \n}', 'CHECKBOX', '', '2019-10-27 20:10:59', 'WULAN', '2019-10-27 20:10:59', 'WULAN');

-- --------------------------------------------------------

--
-- Table structure for table `questionairepage`
--

CREATE TABLE `questionairepage` (
  `QPAGEID` int(11) NOT NULL,
  `QUESTIONNAIREID` int(11) DEFAULT NULL,
  `TITLE` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `SECTIONORDER` varchar(400) DEFAULT NULL,
  `CONDITIONALLOGIC` longtext DEFAULT NULL,
  `CREATEDDATE` datetime DEFAULT NULL,
  `CREATEDBY` varchar(30) DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `UPDATEBY` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionairepage`
--

INSERT INTO `questionairepage` (`QPAGEID`, `QUESTIONNAIREID`, `TITLE`, `DESCRIPTION`, `SECTIONORDER`, `CONDITIONALLOGIC`, `CREATEDDATE`, `CREATEDBY`, `UPDATEDATE`, `UPDATEBY`) VALUES
(1, 1, 'HAL. 1', '', '', '', '2019-10-27 13:45:42', 'WULAN', '2019-10-27 13:45:42', 'WULAN'),
(2, 1, 'HAL. 2', '', '', '', '2019-10-27 13:47:02', 'WULAN', '2019-10-27 13:47:02', 'WULAN'),
(3, 1, 'HAL. 3', '', '', '', '2019-10-27 13:47:02', 'WULAN', '2019-10-27 13:47:02', 'WULAN'),
(4, 3, 'HAL. 1', '', '', '', '2019-10-27 13:47:42', 'WULAN', '2019-10-27 13:47:42', 'WULAN'),
(5, 3, 'HAL. 2', '', '', '', '2019-10-27 13:47:42', 'WULAN', '2019-10-27 13:47:42', 'WULAN'),
(6, 3, 'HAL. 3', '', '', '', '2019-10-27 13:47:42', 'WULAN', '2019-10-27 13:47:42', 'WULAN'),
(7, 2, 'HAL. 1', '', '', '', '2019-10-27 13:47:58', 'WULAN', '2019-10-27 13:47:58', 'WULAN');

-- --------------------------------------------------------

--
-- Table structure for table `questionairesection`
--

CREATE TABLE `questionairesection` (
  `SECTIONID` int(11) NOT NULL,
  `QPAGEID` int(11) DEFAULT NULL,
  `TITLE` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `FIELDORDER` varchar(200) DEFAULT NULL,
  `CONDITIONALLOGIC` longtext DEFAULT NULL,
  `SECTIONOPTIONS` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionairesection`
--

INSERT INTO `questionairesection` (`SECTIONID`, `QPAGEID`, `TITLE`, `DESCRIPTION`, `FIELDORDER`, `CONDITIONALLOGIC`, `SECTIONOPTIONS`) VALUES
(1, 1, 'SEC 1', 'BEKERJA/BELUM BEKERJA', '', '', ''),
(2, 1, 'SEC 2', 'SUDAH BEKERJA', '', '', ''),
(3, 2, 'SEC 5', 'BIDANG INFORMATIKA', '', '', ''),
(4, 3, 'SEC 6', 'INFORMASI BOS', '', '', ''),
(5, 4, 'SEC 1', 'BEKERJA/BELUM BEKERJA', '', '', ''),
(6, 4, 'SEC 2', 'BELUM BEKERJA', '', '', ''),
(7, 4, 'SEC 3', 'SUDAH BEKERJA', '', '', ''),
(8, 5, 'SEC 4', 'BIDANG INFORMATIKA', '', '', ''),
(9, 6, 'SEC 5', 'INFORMASI BOS', '', '', ''),
(10, 7, 'SEC 1', 'KUALITAS', '', '', ''),
(11, 1, 'SEC 3', 'SETELAH LULUS DAPAT KERJA', '', '', ''),
(12, 1, 'SEC 4', 'LEVEL TEMPAT KERJA', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `questionnaire`
--

CREATE TABLE `questionnaire` (
  `QUESTIONNAIREID` int(11) NOT NULL,
  `TITLE` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `ENTRIES` int(11) DEFAULT NULL,
  `CONDITIONALLOGIC` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionnaire`
--

INSERT INTO `questionnaire` (`QUESTIONNAIREID`, `TITLE`, `DESCRIPTION`, `ENTRIES`, `CONDITIONALLOGIC`) VALUES
(1, 'Kuisioner 1 JTK', 'Kuisioner pertama jurusan Teknik Komputer dan Informatika', 0, 'K1'),
(2, 'Kuisioner Pengguna Lulusan', 'Kuisioner pengguna lulusan D3/D4 Jurusan Teknik Komputer dan Informatika', 0, 'PL'),
(3, 'Kuisioner 2 JTK', 'Kuisioner kedua lulusan D3/D4 Jurusan Teknik Komputer dan Informatika', 0, 'K2');

-- --------------------------------------------------------

--
-- Table structure for table `referencecat`
--

CREATE TABLE `referencecat` (
  `REFERENCEID` int(11) NOT NULL,
  `REFERENCENAME` varchar(30) NOT NULL,
  `isAScale` enum('YES','NO') NOT NULL,
  `SCALE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referencecat`
--

INSERT INTO `referencecat` (`REFERENCEID`, `REFERENCENAME`, `isAScale`, `SCALE`) VALUES
(1, 'STATUS BEKERJA', 'NO', 0),
(2, 'MASA TUNGGU SEBELUM', 'NO', 0),
(3, 'MASA TUNGGU SESUDAH', 'NO', 0),
(4, 'LEVEL TEMPAT KERJA', 'NO', 0),
(5, 'POSISI PEKERJAAN', 'NO', 0),
(6, 'BIDANG SEDANG', 'NO', 0),
(7, 'METODE SOFTWARE', 'NO', 0),
(8, 'TESTING METHOD', 'NO', 0),
(9, 'PROJECT TOOLS', 'NO', 0),
(10, 'DB TOOLS', 'NO', 0),
(11, 'BAHASA PEMROGRAMAN', 'NO', 0),
(12, 'FRAMEWORK DEV', 'NO', 0),
(13, 'DB TECHNOLOGY', 'NO', 0),
(14, 'TESTING TOOLS', 'NO', 0),
(15, 'COLLABORATIVE TOOLS', 'NO', 0),
(16, 'STANDARD', 'NO', 0),
(17, 'COURSES', 'NO', 0),
(18, 'COMPANY NAME', 'NO', 0),
(19, 'COMPANY ADDRESS', 'NO', 0),
(20, 'COMPANY PHONE', 'NO', 0),
(21, 'COMPANY EMAIL', 'NO', 0),
(22, 'BOSS NAME', 'NO', 0),
(23, 'BOSS JOB', 'NO', 0),
(24, 'BOSS PHONE', 'NO', 0),
(25, 'BOSS EMAIL', 'NO', 0),
(26, 'ALASAN TIDAK BEKERJA', 'NO', 0),
(27, 'PORTOFOLIO', 'NO', 0),
(28, 'PELATIHAN', 'NO', 0),
(29, 'KUALITAS', 'YES', 4),
(30, 'KEPUASAN KOMPETENSI', 'YES', 4);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `ROLEID` int(11) NOT NULL,
  `ROLENAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`ROLEID`, `ROLENAME`, `DESCRIPTION`) VALUES
(1, 'Admin', NULL),
(2, 'Koordinator Pelaksana', NULL),
(3, 'Operator', NULL),
(4, 'Surveyor D3', NULL),
(5, 'Lulusan', NULL),
(6, 'Surveyor D4', NULL),
(7, 'Pengguna Lulusan', NULL),
(8, 'Pengolah Data Tracer Studi', NULL),
(9, 'Tim Akreditasi', NULL),
(10, 'Kepala Jurusan', NULL),
(11, 'Kepala Prodi', NULL),
(12, 'JPAC', NULL),
(13, 'Tim Pengembang Kurikulum', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roleinfunction`
--

CREATE TABLE `roleinfunction` (
  `roleid` int(11) NOT NULL,
  `functionid` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roleinfunction`
--

INSERT INTO `roleinfunction` (`roleid`, `functionid`) VALUES
(1, 'MF01'),
(2, 'TF01'),
(2, 'TF02'),
(2, 'TF03'),
(2, 'TF04'),
(2, 'TF05'),
(2, 'TF06'),
(2, 'TF07'),
(2, 'TF08'),
(2, 'TF09'),
(2, 'TF10'),
(8, 'TF02'),
(8, 'TF03'),
(8, 'TF04'),
(8, 'TF05'),
(8, 'TF06'),
(8, 'TF07'),
(8, 'TF08'),
(8, 'TF09'),
(8, 'TF10'),
(9, 'TF01'),
(9, 'TF02'),
(9, 'TF03'),
(9, 'TF04'),
(9, 'TF05'),
(9, 'TF06');

-- --------------------------------------------------------

--
-- Stand-in structure for view `scaleradiolabel`
-- (See below for the actual view)
--
CREATE TABLE `scaleradiolabel` (
`REFERENCEID` int(11)
,`SCALE` int(11)
,`label` text
);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `USERID` int(11) NOT NULL,
  `USERNAME` varchar(20) NOT NULL,
  `PASSWORD` varchar(191) NOT NULL,
  `PASSWORDSALT` varchar(15) NOT NULL,
  `LASTLOGIN` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `CREATEDDATE` datetime DEFAULT NULL,
  `CREATEDBY` varchar(30) DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `UPDATEBY` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`USERID`, `USERNAME`, `PASSWORD`, `PASSWORDSALT`, `LASTLOGIN`, `CREATEDDATE`, `CREATEDBY`, `UPDATEDATE`, `UPDATEBY`) VALUES
(1, 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'Password', '2019-11-06 08:48:40', NULL, NULL, NULL, NULL),
(2, 'Surveyor', 'Password', 'Password', '2020-01-20 07:13:19', NULL, NULL, '2020-01-20 07:13:19', 'Admin'),
(3, 'Jurusan', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(4, 'Alumni1', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(5, 'Alumni2', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(6, 'Alumni3', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(7, 'Alumni4', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(8, 'Alumni5', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(9, 'Alumni6', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(10, 'Alumni7', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(11, 'Alumni8', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(12, 'Alumni9', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(13, 'Alumni10', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(14, 'Alumni11', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(15, 'Alumni12', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(16, 'Alumni13', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(17, 'Alumni14', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(18, 'Alumni15', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(19, 'Alumni16', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(20, 'Alumni17', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(21, 'Alumni18', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(22, 'Alumni19', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(23, 'Alumni20', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(24, 'Alumni21', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(25, 'Alumni22', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(26, 'Alumni23', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(27, 'Alumni24', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(28, 'Alumni25', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(29, 'Alumni26', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(30, 'Alumni27', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(31, 'Alumni28', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(32, 'Alumni29', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(33, 'Alumni30', 'Password', 'Password', '2019-10-27 09:23:40', NULL, NULL, NULL, NULL),
(34, 'Alumni30', 'Alumni30', '', '2019-11-13 01:48:44', NULL, NULL, NULL, NULL),
(41, 'Asri', '827ccb0eea8a706c4c34a16891f84e7b', '', '2020-01-20 07:07:17', '2020-01-20 07:07:17', 'Admin', '2020-01-20 07:07:17', 'Admin'),
(42, 'wulan', '827ccb0eea8a706c4c34a16891f84e7b', '', '2020-01-20 07:08:37', '2020-01-20 07:08:37', 'Admin', '2020-01-20 07:08:37', 'Admin'),
(43, 'rini', '827ccb0eea8a706c4c34a16891f84e7b', '', '2020-01-20 07:09:01', '2020-01-20 07:09:01', 'Admin', '2020-01-20 07:09:01', 'Admin'),
(44, 'surveyorcici', '827ccb0eea8a706c4c34a16891f84e7b', '', '2020-01-20 07:13:58', '2020-01-20 07:13:58', 'Admin', '2020-01-20 07:13:58', 'Admin'),
(45, 'abah', '827ccb0eea8a706c4c34a16891f84e7b', '', '2020-01-20 07:14:20', '2020-01-20 07:14:20', 'Admin', '2020-01-20 07:14:20', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `userinrole`
--

CREATE TABLE `userinrole` (
  `USERINROLEID` int(11) NOT NULL,
  `USERID` int(11) NOT NULL,
  `ROLEID` int(11) NOT NULL,
  `CREATEDDATE` datetime DEFAULT NULL,
  `CREATEDBY` varchar(30) DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `UPDATEBY` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinrole`
--

INSERT INTO `userinrole` (`USERINROLEID`, `USERID`, `ROLEID`, `CREATEDDATE`, `CREATEDBY`, `UPDATEDATE`, `UPDATEBY`) VALUES
(1, 1, 1, NULL, NULL, NULL, NULL),
(2, 2, 6, NULL, NULL, NULL, NULL),
(3, 3, 3, NULL, NULL, NULL, NULL),
(4, 4, 5, NULL, NULL, NULL, NULL),
(5, 5, 5, NULL, NULL, NULL, NULL),
(6, 6, 5, NULL, NULL, NULL, NULL),
(7, 7, 5, NULL, NULL, NULL, NULL),
(8, 8, 5, NULL, NULL, NULL, NULL),
(9, 9, 5, NULL, NULL, NULL, NULL),
(10, 10, 5, NULL, NULL, NULL, NULL),
(11, 11, 5, NULL, NULL, NULL, NULL),
(12, 12, 5, NULL, NULL, NULL, NULL),
(13, 13, 5, NULL, NULL, NULL, NULL),
(14, 14, 5, NULL, NULL, NULL, NULL),
(15, 15, 5, NULL, NULL, NULL, NULL),
(16, 16, 5, NULL, NULL, NULL, NULL),
(17, 17, 5, NULL, NULL, NULL, NULL),
(18, 18, 5, NULL, NULL, NULL, NULL),
(19, 19, 5, NULL, NULL, NULL, NULL),
(20, 20, 5, NULL, NULL, NULL, NULL),
(21, 21, 5, NULL, NULL, NULL, NULL),
(22, 22, 5, NULL, NULL, NULL, NULL),
(23, 23, 5, NULL, NULL, NULL, NULL),
(24, 24, 5, NULL, NULL, NULL, NULL),
(25, 25, 5, NULL, NULL, NULL, NULL),
(26, 26, 5, NULL, NULL, NULL, NULL),
(27, 27, 5, NULL, NULL, NULL, NULL),
(28, 28, 5, NULL, NULL, NULL, NULL),
(29, 29, 5, NULL, NULL, NULL, NULL),
(30, 30, 5, NULL, NULL, NULL, NULL),
(31, 31, 5, NULL, NULL, NULL, NULL),
(32, 32, 5, NULL, NULL, NULL, NULL),
(33, 33, 5, NULL, NULL, NULL, NULL),
(34, 41, 2, '2020-01-20 07:07:17', 'Admin', '2020-01-20 07:07:17', 'Admin'),
(35, 42, 8, '2020-01-20 07:08:37', 'Admin', '2020-01-20 07:08:37', 'Admin'),
(36, 43, 9, '2020-01-20 07:09:01', 'Admin', '2020-01-20 07:09:01', 'Admin'),
(37, 44, 6, '2020-01-20 07:13:58', 'Admin', '2020-01-20 07:13:58', 'Admin'),
(38, 45, 5, '2020-01-20 07:14:20', 'Admin', '2020-01-20 07:14:20', 'Admin');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_masatunggu`
-- (See below for the actual view)
--
CREATE TABLE `v_masatunggu` (
`TAHUNLULUS` int(4)
,`JMLLULUSAN` int(11)
,`JMLTERLACAK` int(11)
,`PRODID` int(11)
,`SEBELUMLULUS` bigint(21)
,`BULAN1` bigint(21)
,`BULAN2` bigint(21)
,`BULAN3` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_masatunggupie`
-- (See below for the actual view)
--
CREATE TABLE `v_masatunggupie` (
`ANSWER` longtext
,`PRODID` int(11)
,`COUNT` bigint(21)
,`PERSEN` double(5,2)
);

-- --------------------------------------------------------

--
-- Structure for view `scaleradiolabel`
--
DROP TABLE IF EXISTS `scaleradiolabel`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `scaleradiolabel`  AS  select `r`.`REFERENCEID` AS `REFERENCEID`,`r`.`SCALE` AS `SCALE`,json_extract(`q`.`QUESTION`,'$.SubQuestion[*].Question') AS `label` from (`questionairefield` `q` join `referencecat` `r`) where `q`.`REFERENCEID` = `r`.`REFERENCEID` ;

-- --------------------------------------------------------

--
-- Structure for view `v_masatunggu`
--
DROP TABLE IF EXISTS `v_masatunggu`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_masatunggu`  AS  select year(`p`.`GRADUATIONDATE`) AS `TAHUNLULUS`,`JML_LULUSAN`(`p`.`GRADUATIONDATE`,`p`.`PRODID`) AS `JMLLULUSAN`,`JML_TERLACAK`(`p`.`GRADUATIONDATE`,`p`.`PRODID`) AS `JMLTERLACAK`,`p`.`PRODID` AS `PRODID`,count(case `a`.`ANSWER` when 'SEBELUM LULUS' then 1 else NULL end) AS `SEBELUMLULUS`,count(case `a`.`ANSWER` when '0-3 BULAN' then 1 else NULL end) AS `BULAN1`,count(case `a`.`ANSWER` when '3-6 BULAN' then 1 else NULL end) AS `BULAN2`,count(case `a`.`ANSWER` when '> 6 BULAN' then 1 else NULL end) AS `BULAN3` from (((`answer` `a` join `alumnprofile` `p` on(`p`.`USERID` = `a`.`USERID`)) join `questionairefield` `q` on(`q`.`FIELDID` = `a`.`FIELDID`)) join `referencecat` `r` on(`q`.`REFERENCEID` = `r`.`REFERENCEID`)) where `q`.`REFERENCEID` = 2 or `q`.`REFERENCEID` = 3 and year(`p`.`GRADUATIONDATE`) >= year(curdate()) - 5 group by year(`p`.`GRADUATIONDATE`),`p`.`PRODID` ;

-- --------------------------------------------------------

--
-- Structure for view `v_masatunggupie`
--
DROP TABLE IF EXISTS `v_masatunggupie`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_masatunggupie`  AS  select `a`.`ANSWER` AS `ANSWER`,`p`.`PRODID` AS `PRODID`,count(`a`.`ANSWER`) AS `COUNT`,`GET_PERCENTAGE`(count(`a`.`ANSWER`) / `JML_TERLACAK_5THN`(`p`.`PRODID`)) AS `PERSEN` from (((`answer` `a` join `alumnprofile` `p` on(`p`.`USERID` = `a`.`USERID`)) join `questionairefield` `q` on(`q`.`FIELDID` = `a`.`FIELDID`)) join `referencecat` `r` on(`q`.`REFERENCEID` = `r`.`REFERENCEID`)) where (`q`.`REFERENCEID` = 2 or `q`.`REFERENCEID` = 3) and `a`.`ANSWER` <> 'SESUDAH LULUS' and year(`p`.`GRADUATIONDATE`) >= year(curdate()) - 5 group by `a`.`ANSWER`,`p`.`PRODID` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumnprofile`
--
ALTER TABLE `alumnprofile`
  ADD PRIMARY KEY (`PROFILEID`),
  ADD KEY `FK_PRODI_ALUMN` (`PRODID`),
  ADD KEY `FK_USER_ALUMN` (`USERID`),
  ADD KEY `idx_graduationdate` (`GRADUATIONDATE`);

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`ANSWERID`),
  ADD KEY `FK_ANSWER_USER` (`USERID`);

--
-- Indexes for table `bossprofile`
--
ALTER TABLE `bossprofile`
  ADD PRIMARY KEY (`BOSSID`),
  ADD KEY `FK_ALUMNBOSS` (`PROFILEID`),
  ADD KEY `FK_USERBOSS` (`USERID`);

--
-- Indexes for table `major`
--
ALTER TABLE `major`
  ADD PRIMARY KEY (`MAJORID`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`PRODID`);

--
-- Indexes for table `questionairefield`
--
ALTER TABLE `questionairefield`
  ADD PRIMARY KEY (`FIELDID`),
  ADD KEY `FK_QSEC_QFIELD` (`SECTIONID`),
  ADD KEY `REFERENCEID` (`REFERENCEID`);

--
-- Indexes for table `questionairepage`
--
ALTER TABLE `questionairepage`
  ADD PRIMARY KEY (`QPAGEID`),
  ADD KEY `FK_Q_QPAGE` (`QUESTIONNAIREID`);

--
-- Indexes for table `questionairesection`
--
ALTER TABLE `questionairesection`
  ADD PRIMARY KEY (`SECTIONID`),
  ADD KEY `FK_QPAGE_QSEC` (`QPAGEID`);

--
-- Indexes for table `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`QUESTIONNAIREID`);

--
-- Indexes for table `referencecat`
--
ALTER TABLE `referencecat`
  ADD PRIMARY KEY (`REFERENCEID`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`ROLEID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`USERID`);

--
-- Indexes for table `userinrole`
--
ALTER TABLE `userinrole`
  ADD PRIMARY KEY (`USERINROLEID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumnprofile`
--
ALTER TABLE `alumnprofile`
  MODIFY `PROFILEID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `ANSWERID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=480;

--
-- AUTO_INCREMENT for table `bossprofile`
--
ALTER TABLE `bossprofile`
  MODIFY `BOSSID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `major`
--
ALTER TABLE `major`
  MODIFY `MAJORID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prodi`
--
ALTER TABLE `prodi`
  MODIFY `PRODID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `questionairefield`
--
ALTER TABLE `questionairefield`
  MODIFY `FIELDID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `questionairepage`
--
ALTER TABLE `questionairepage`
  MODIFY `QPAGEID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `questionairesection`
--
ALTER TABLE `questionairesection`
  MODIFY `SECTIONID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `QUESTIONNAIREID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `referencecat`
--
ALTER TABLE `referencecat`
  MODIFY `REFERENCEID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `ROLEID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `USERID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `userinrole`
--
ALTER TABLE `userinrole`
  MODIFY `USERINROLEID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `questionairefield`
--
ALTER TABLE `questionairefield`
  ADD CONSTRAINT `FK_QSEC_QFIELD` FOREIGN KEY (`SECTIONID`) REFERENCES `questionairesection` (`SECTIONID`),
  ADD CONSTRAINT `questionairefield_ibfk_1` FOREIGN KEY (`REFERENCEID`) REFERENCES `referencecat` (`REFERENCEID`);

--
-- Constraints for table `questionairepage`
--
ALTER TABLE `questionairepage`
  ADD CONSTRAINT `FK_Q_QPAGE` FOREIGN KEY (`QUESTIONNAIREID`) REFERENCES `questionnaire` (`QUESTIONNAIREID`);

--
-- Constraints for table `questionairesection`
--
ALTER TABLE `questionairesection`
  ADD CONSTRAINT `FK_QPAGE_QSEC` FOREIGN KEY (`QPAGEID`) REFERENCES `questionairepage` (`QPAGEID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
