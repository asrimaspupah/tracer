<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\ModelUser;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('user', function (Blueprint $table) {
            $table->string('nik', 10)->primary();
            $table->string('nama');
            $table->bigInteger('role');
            $table->string('password');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });

        ModelUser::create(
            ['nik' => '007',
             'nama' => 'Admin',
             'password' => md5('bsp123!!'),
             'role' => 0,
             'created_by' => 'Admin',
             'updated_by' => 'Admin']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
