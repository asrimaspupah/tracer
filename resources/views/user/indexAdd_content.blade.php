<!-- page content -->
 <div class="right_col" role="main">
                <div id="breadcrumbs-wrapper">
                    <ol class="breadcrumb breadcrumb-bg">
                        <li><i class="fa fa-users"></i> Users <span style="margin:0 10px;">></span>
                            <i class="fa fa-database"></i><a href="{{ url('/user') }}"
                                style="text-decoration: none;"> User</a> <span
                                style="margin:0 10px;">></span>
                            <i class="fa fa-user"></i><a href="{{ url('/user/add') }}"
                                style="text-decoration: none;"> Add User</a> </li>
                    </ol>
                </div>
                <div class="">
                    <div class="clearfix"></div>


                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 pull-left" style="padding: 0 10px;">
                                                <span><i>Data user from BAS</i></span>
                                            </div>
                                            <div class="col-md-6 pull-right" style="text-align: right; padding:0 5px;">
                                                <input id="input-search"  class="search" type="text" placeholder="Search"
                                                    style="display: inline-block; width: 200px;">
                                                <button type="button" class="btn"
                                                    style="background-color: #2A3F54; color: white"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                        <table id="dtBasicExample" class="table table-striped jambo_table bulk_action table-bordered mytable">
                                            <thead>
                                                <tr class="headings">
                                                    <th class="column-title text-center" >Name</th>
                                                    <th class="column-title text-center">NIK </th>
                                                    <th class="column-title text-center">ROLE </th>
                                                    <th class="column-title text-center" style="width: 200px">Action </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($user as $json)
                                                    <tr>
                                                        <td class=" " style="vertical-align: middle">{{$json->NAME}}</td>
                                                        <td class="text-right" style="vertical-align: middle">{{$json->NIK}}</td>
                                                        <td class="text-center" style="vertical-align: middle">-</td>
                                                        <td class="text-center" style="vertical-align: middle">
                                                            <button type="button" onclick="addUser('{{$json->NIK}}')" class="btn btn-sm" style="background-color: #2A3F54; color: white"><i
                                                                class="fa fa-plus"></i> Add To User</button>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /page content -->
    
<!-- MODAL -->
<div class="modal fade" id="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 data-brackets-id="10786" class="modal-title" id="komponen-title">Change</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <form id="forms" action="/user/add/" method="POST">
                            @csrf
                            <input name="nik" id="nik" type="hidden" />
                            <div class="group" id="select">
                                <select name="role" id="role">
                                    @foreach ($masterRole as $item)
                                        <option value="{{$item->id}}" rel="icon-temperature">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <button type="button" onclick="addData()" class="btn pull-right" style="background-color: #2A3F54 !important; color : white;"><i class="fa fa-save" style="color: white"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /MODAL -->
    {{-- Javascript --}}

<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
<script>
$( document ).ready(function() {
    $('#dtBasicExample').DataTable({ 
        "sDom":"ltipr" ,
        "bLengthChange": false,
    });
    var table = $('#dtBasicExample').DataTable();
    $('#input-search').on( 'keyup', function () {
    table
        .search( this.value )
        .draw();
    });
});
</script>
{{-- Javascript --}}