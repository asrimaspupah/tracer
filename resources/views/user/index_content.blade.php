<!-- page content -->
<div class="right_col" role="main">
    <div id="breadcrumbs-wrapper">
        <ol class="breadcrumb breadcrumb-bg">
            <li><i class="fa fa-users"></i> Users <span style="margin:0 10px;">></span> <i class="fa fa-database"></i><a href="{{ url('/user') }}" style="text-decoration: none;"> User List</a></li>
        </ol>
    </div>
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 pull-left" style="padding: 0 10px;">
                                    <button type="button" onclick="showModalAdd()"  class="btn pull-left" style="background-color: #2A3F54; color: white"><i class="fa fa-plus"></i> Add New User</button>
                                </div>
                                <div class="col-md-6 pull-right" style="text-align: right; padding:0 5px;">
                                    <input id="input-search" class="search"  type="text" placeholder="Search" style="display: inline-block; width: 200px;">
                                    <button type="button" class="btn" style="background-color: #2A3F54; color: white"><i class="fa fa-search"></i></button>
                                </div>
                                </div>
                                    <table id="dtBasicExample" class="table table-striped jambo_table bulk_action table-bordered mytable">
                                        <thead>
                                            <tr class="headings">
                                                <th class="column-title text-center">Id </th>
                                                <th class="column-title text-center" style="width: 200px">Name</th>
                                                <th class="column-title text-center">Role </th>
                                                <th class="column-title text-center">Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- /page content -->
<!-- MODAL -->
<div class="modal fade" id="modal" role="dialog">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 data-brackets-id="10786" class="modal-title" id="komponen-title">Change</h4>
            </div>
            <div class="modal-body">
                <div>
                    <form id="forms" action="" method="POST" class="form-horizontal form-label-left">
                        <input id="mode" type = "hidden"/>
                        <input id="currentId" type = "hidden"/>
                        <!-- <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="id">User Id <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="userid" name="userid" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">User Name <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="role">Role Name <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12 ">
                                <select name="role" id="userrole" class="form-control">
                                <option value="">Select</option>
                                    @foreach ($masterRole as $item)
                                        <option value="{{$item->ROLEID}}">{{$item->ROLENAME}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="button" onclick="doSave()" class="btn pull-right" tabindex="5" style="background-color: #26B99A !important; color : white;"> Save</button>
                            <button type="button" onclick="" class="btn pull-right" tabindex="5" style="background-color: #2A3F54 !important; color : white;">Reset</button>
                            <button type="button" onclick="" class="btn pull-right" tabindex="5" style="background-color: #2A3F54 !important; color : white;">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /MODAL -->

{{-- Javascript --}}

<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
<script>
var APP_URL = {!! json_encode(url('/')) !!}
var USER_LOGIN = {!! json_encode(Session::get('username')) !!}
$( document ).ready(function() {
    doSearch();
    $('#input-search').on( 'keyup', function () {
       doSearch();
    });
});
function doSearch(){
    $.ajax({
        type: "GET",
        url: APP_URL + "/api/user/search", 
        data: { 
        'keyword': $('#input-search').val() 
        },
        success: function(result){
            $("#dtBasicExample tbody").empty();
            var data = result.data;
            var rowData = "";
            for (var index = 0; index < data.length ; index++){
                rowData = rowData + "<tr>";
                rowData = rowData + "<td class='text-center' style='vertical-align: middle; text-align: left;'>"+ data[index].USERID+" </td>";
                rowData = rowData + "<td class=' ' style='vertical-align: middle'>"+data[index].USERNAME+"</td>";
                rowData = rowData + "<td class=' ' style='vertical-align: middle'>"+data[index].ROLENAME+"</td>";
                rowData = rowData + "<td class='text-center' style='vertical-align: middle'>";
                    rowData = rowData + "<button type='button' onclick='doDelete(\""+data[index].USERID+"\")' class='btn btn-sm' style='background-color: #2A3F54 !important; '><i class='fa fa-trash' style='color: white'></i></button>";
                    rowData = rowData + "<button onclick='showModalEdit(\""+data[index].USERID+"\")' type='button' class='btn btn-ubah btn-sm' style='background-color: #2A3F54 !important;'><i class='fa fa-pencil' style='color: white'></i></button>";
                rowData = rowData + "</td>";
                rowData = rowData + "</tr>";
            }
            $("#dtBasicExample tbody").append(rowData);
        }
    });
} 
function showModalAdd() {
    $(".modal-title").html("Add");
    $("#mode").val("POST");
    showModal();
}
function showModalEdit(id) {
    $("#mode").val("PUT");
    $(".modal-title").html("Change");
    $('#currentId').val(id)
    $.ajax({
        type: "GET",
        url: APP_URL + "/api/user/getById" , 
        data: { 
        'userid': id
        },
        success: function(result, status){
           var data = result.data;
           $('#userid').val(data.USERID);
           $('#username').val(data.USERNAME);
           $('#userrole').val(data.ROLEID);
        }
    });
    showModal();
}

function doSave(){
    $.ajax({
        type: $("#mode").val(),
        url: APP_URL + "/api/user/save" , 
        data: { 
        //'userid': $('#userid').val(),
        'username': $('#username').val(),
        'userrole': $('#userrole').val(),
        'creator': USER_LOGIN,
        'currentId': $('#currentId').val() 
        },
        success: function(result, status){
           alert(status);
           $('#modal').modal('hide');
           doSearch();
        }
    });
} 
function doDelete(id){
    
    $.ajax({
        type: "DELETE",
        url: APP_URL + "/api/user/delete" , 
        data: { 
        'userid': id
        },
        success: function(result, status){
           alert(status);
           doSearch();
        }
    });
} 
</script>
{{-- Javascript --}}