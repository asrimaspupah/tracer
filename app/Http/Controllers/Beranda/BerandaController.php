<?php

namespace App\Http\Controllers\Beranda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BerandaController extends Controller
{
    public function index(){
        if (!Session::get('login')) {
            return redirect('/')->with('alert', 'Kamu harus login dulu');
        }else{
            return view('beranda/index');
        }
    }
}
