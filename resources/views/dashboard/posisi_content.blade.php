 <div class="right_col" role="main">
     <div class="container-fluid">
         <div id="breadcrumbs-wrapper">
             <ol class="breadcrumb breadcrumb-bg">
                 <li><i class="fa fa-users"></i><a href="{{ url('/dasbor') }}" style="text-decoration: none;"> Dashboard <span style="margin:0 10px;">></span> <i
                         class="fa fa-database"></i><a href="{{ url('/dasbor/posisi') }}" style="text-decoration: none;"> Kesesuaian Profil Lulusan</a></li>
             </ol>
         </div>
         <div class="row clearfix">
             <!-- Bar Chart Kepuasan-->
             <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                 <div class="card">
                     <div class="header">
                         <h2>Kesesuaian antara Bidang Keilmuan dengan Pekerjaan Lulusan</h2>
                         <ul class="header-dropdown m-r--5">
                             <li class="dropdown">
                                 <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                     role="button" aria-haspopup="true" aria-expanded="false">
                                     <i class="material-icons">more_vert</i>
                                 </a>
                                 <ul class="dropdown-menu pull-right">
                                     <li><a href="javascript:void(0);">Unduh</a></li>
                                 </ul>
                             </li>
                         </ul>
                     </div>
                     <div class="body">
                         <canvas id="barChart" ></canvas>
                     </div>
                 </div>
             </div>
             <!-- #END# Bar Chart Kepuasan-->
             <!-- Filtering -->
             <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                 <div class="card">
                     <div class="body">
                         <div class="">
                             <p><b>Jurusan</b></p>
                             <select id="major" onchange="getProdi();" class="form-control show-tick">
                                 <!-- <option value="">-- Pilih Jurusan --</option> -->
                                 @foreach ($major as $item)
                                 <option value="{{$item->MAJORID}}">{{$item->MAJORNAME}}</option>
                                 @endforeach
                             </select>
                         </div>
                         <div class="m-t-15">
                             <p><b>Program Studi</b></p>
                             <select id="prodidropdown" class="form-control show-tick" onchange="getStackBar(this);">
                                 <!-- <option value="">-- Pilih Program Studi --</option> -->
                             </select>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Filtering -->
             <!-- Exportable Table -->
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="card">
                     <div class="header">
                         <h2>
                             Data detail
                         </h2>
                     </div>
                     <div class="body">
                         <div class="table-responsive">
                             <table
                                 class="table table-bordered table-striped table-hover dataTable js-exportable table-custom">
                                 <thead>
                                    <tr>
                                         <th rowspan="2" class="col-md-1">Tahun</th>
                                         <th rowspan="2" class="col-md-1">Jumlah Lulusan</th>
                                         <th rowspan="2" class="col-md-2">Jumlah Lulusan yang Terlacak</th>
                                         <th rowspan="2" class="col-md-2">Jumlah Lulusan Berpindah Pekerjaan</th>
                                         <th colspan="3">Jumlah lulusan Terlacak dengan Tingkat Keseuaian Bidang Kerja</th>
                                     </tr>
                                     <tr>
                                         <th class="col-md-2">Tinggi</th>
                                         <th class="col-md-2">Sedang</th>                                         
                                         <th class="col-md-2">Rendah</th>  
                                     </tr>
                                 </thead>
                                 <tbody id="mtMod">

                                 </tbody>
                             </table>
                         </div>
                     </div>
                 </div> -->
                <!-- #END# Exportable Table-->
                <!-- Modal -->
                <div class="modal" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" id="modalname"></h4>
                            </div>
                            <div class="modal-body">
                                <canvas id="detailline_chart"></canvas>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Modal -->
             </div>
         </div>
     </div>
 </div>

 <!-- /page content -->

 {{-- Javascript --}}
 <script src="{{ asset('bsb/plugins/jquery/jquery.min.js') }}"></script>

 <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
    var myChart = null;
    var myTable = {};
    var myDetailChart = null;
    var dataDetail = null;
    var labelyear = [];

    $(function() {
        $("#major").trigger("change");

        myTable = $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'excel', 'pdf'
            ]
        });
    });

    function getProdi() {
        $.ajax({
            type: "GET",
            url: APP_URL + "/api/dasbor/cmbprodi",
            data: {
                'major': $('#major').val()
            },
            success: function(result) {
                var element = $('#prodidropdown');
                element.empty();
                // element.append("<option value=''> -- Pilih Program Studi -- </option>");
                var prodi = result.data;
                for (var i = 0; i < prodi.length; i++) {
                    // POPULATE SELECT ELEMENT WITH JSON.
                    element.append("<option value='" + prodi[i].PRODID + "'>" + prodi[i].PRODINAME +
                        "</option>");
                }
                $('#prodidropdown').trigger("change");
            }
        });
    }

    function getStackBar(sel) {
        if (myChart != null) {
            myChart.destroy();
        }
        myTable.destroy();
        $("#mtMod").html('');

        var datalabel = [];  
        var tableString = "";

        $.ajax({
            type: "GET",
            url: APP_URL + "/api/dasbor/posisi/",
            data: {
                'prodi': $('#prodidropdown').val()
            },
            success: function(result) {
                dataDetail = result;
                var dataset = [];
                var nilai = [];
                var pindahkerja = [];
                var year;
                labelyear = [];
                var percent = 0;
                var bgColor = ["#64e291","#fec771", "#e6e56c" ];
                var hbgColor = ["#5ccc84","#e0b065", "#cfce63"];
                datalabel = ["Tinggi", "Sedang", "Rendah"];
                datalabelb = ["TINGGI", "SEDANG", "RENDAH"];
                nilaiTinggi = [];

                for (var i = 0; i < result.posisi.length; i++){                
                    labelyear.push(result.posisi[i].TAHUN);
                }

                for (var i = 0; i < datalabelb.length; i++){
                    var data = [];                
                    for (var j = 0; j < result.posisi.length; j++){
                        if (datalabelb[i] === "TINGGI"){
                            nilai = result.posisi[j]["PINDAHKERJA"] - (result.posisi[j]["RENDAH"]+result.posisi[j]["SEDANG"]);
                            nilaiTinggi.push(nilai);
                        } else {
                            nilai = result.posisi[j][datalabelb[i]];
                        }       
                        pindahkerja = result.posisi[j]["PINDAHKERJA"];
                        percent = (nilai/pindahkerja)*100;
                        data.push(percent);
                    }
                    dataset.push({label:datalabel[i], data:data, backgroundColor:bgColor[i], hoverBackground:hbgColor[i]});
                }

                for (var i = 0; i < result.posisi.length; i++){
                    tableString = tableString + "<tr>";
                    tableString = tableString + "<td class='col-md-1'>" + result.posisi[i].TAHUN +
                        "</td><td class='col-md-1'>" + result.posisi[i].LULUSAN +
                        "</td><td class='col-md-1'>" + result.posisi[i].TERLACAK +
                        "</td><td class='col-md-1'>" + result.posisi[i].PINDAHKERJA + "</td>";
                        for (var j = 0; j < datalabel.length; j++){
                            if (datalabelb[j] === "TINGGI"){
                                tableString = tableString + "<td class='col-md-1'>" + nilaiTinggi[i] + "</td>";
                            } else {
                                tableString = tableString + "<td class='col-md-1'>" + result.posisi[i][datalabelb[j]] + "</td>";
                            }
                            
                        }
                    tableString = tableString + "</tr>";    
                }

                var ctx = document.getElementById("barChart");
                myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: labelyear,
                        datasets: dataset
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                stacked: true
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }            
                });

                $("#mtMod").append(tableString);
                myTable = $('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'excel', 'pdf'
                    ]
                });

                // getBarDetail(barChartData, dataDetail);
            }
        });        
    }


    document.getElementById("barChart").onclick = function(evt)
    {   
        if (myDetailChart != null) {
            myDetailChart.destroy();
        }
        var activePoints = myChart.getElementsAtEvent(evt);

        if(activePoints.length > 0 && dataDetail != null);
        {
            //get the internal index of slice in pie chart
            var clickedElementindex = activePoints[0]["_index"];

            //get specific label by index 
            var label = myChart.data.labels[clickedElementindex];

            $("#myModal").modal();
            $("#modalname").text("Detail Posisi Pekerjaan Lulusan Tahun " + label);

            myDetailChart = new Chart(document.getElementById("detailline_chart").getContext("2d"), getBarDetailConfig(dataDetail, label));
        }
    }

    function getBarDetailConfig(dataDetail, year) {
        var config = null;
        if (dataDetail != null){
            var idx;
            var datal = [];
            var mylabel = JSON.parse(dataDetail.label[0].label);
            for (var i = 0; i < dataDetail.posisi.length; i++){
                if (dataDetail.posisi[i].TAHUN === year){
                    idx = i;
                    break;
                }
            }

            for (var i = 0; i < mylabel.length; i++){
                datal.push(dataDetail.posisi[idx][mylabel[i]]);
            }

            console.log(datal);
            config = {
                type: 'bar',
                data: {
                    labels: mylabel,
                    datasets: [{
                        data: datal,        
                        backgroundColor: ["#D4864F","#DCDE9F","#87B4A5","#279071","#152A36","#7EADC5","#A7D9DD","#D8DBCE","#E7BD79","#EA5342"]
                    }]
                },
                options: {
                    legend: false,
                    scales: {
                    yAxes: [{
                        ticks: {
                            stepSize: 1,
                        }
                    }],
                    xAxes: [{
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90
                    }]
                }
                }
            }

            return config;
        }        
    }
 </script>
 {{-- Javascript --}}