<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'\..\..\resources\views','view');
        $this->loadViewsFrom(__DIR__.'\..\..\resources\views\Payroll\Master','Master');
        $this->loadViewsFrom(__DIR__.'\..\..\resources\views\Payroll\Component','Component');
        $this->loadViewsFrom(__DIR__.'\..\..\resources\views\Payroll\Run','Run');
        $this->loadViewsFrom(__DIR__.'\..\..\resources\views\Payroll\Schedule','Schedule');
        $this->loadViewsFrom(__DIR__.'\..\..\resources\views\User','User');
        $this->loadViewsFrom(__DIR__.'\..\..\resources\views\Core','Core');
        $this->loadViewsFrom(__DIR__.'\..\..\resources\views\Login','Login');

    }
}
