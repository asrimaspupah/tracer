<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ModelComponents;

class CreateMasterComponentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('master_component', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('component_name');
            $table->string('component_type');
            $table->string('component_unit');
            $table->integer('from')->nullable();
            $table->integer('component_value');
            $table->string('component_description');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });

        ModelComponents::create(
            ['id' => 'PP_1',
            'component_name' => 'Pajak', 
            'component_type' => 'Deduction', 
            'component_unit' => 'Percentage',
            'component_value' => 0,
            'component_description' => '-',
            'created_by' => 'Admin',
            'updated_by' => 'Admin']
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_component');
    }
}
