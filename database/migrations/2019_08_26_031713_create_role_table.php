<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role', function (Blueprint $table) {
            $table->string('id', 5)->primary();
            $table->string('name');
            $table->timestamps();
        });

        DB::table('role')->insert(
            [
                [
                    'id' => '0',
                    'name' => 'Admin'
                ],
                [
                    'id' => '1',
                    'name' => 'CEO'
                ],
                [
                    'id' => '2',
                    'name' => 'Staff HRD'
                ],
                [
                    'id' => '3',
                    'name' => 'HRD Manager'
                ]
            ]

        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role');
    }
}
