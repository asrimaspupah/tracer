<?php
 
namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
class DashboardController extends Controller
{
    public function index(){
        $major = DB::table('major')->get();
        return view('dashboard/index',["major"=>$major]);
    }

    public function masaTunggu(){ 
    	// mengambil data masa tunggu alumni dari view masa tunggu lulusan
        $major = DB::table('major')->get();
        return view('dashboard/index',["major"=>$major]);
    }

    public function kepuasan(){ 
    	// mengambil data masa tunggu alumni dari view masa tunggu lulusan
        $major = DB::table('major')->get();
        return view('dashboard/kepuasan',["major"=>$major]);
    }

    public function levelPerusahaan(){
        $major = DB::table('major')->get();
        return view('dashboard/level',["major"=>$major]);
    }

    public function posisiPekerjaan(){
        $major = DB::table('major')->get();
        return view('dashboard/posisi',["major"=>$major]);
    }
}